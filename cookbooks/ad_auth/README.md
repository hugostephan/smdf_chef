# Active Directory Authentification Cookbook

This cookbook installs and configures Active Directory domain authentication for Linux CentOS hosts based on SSSD and Samba.

## Requirements

This cookbook only requires CentOS host architecture with standard repositories configured and a working internet connection.

### Platforms

- CentOS

### Chef

- Chef 12.0 or later

## Attributes

Active Directory group and user name must be in lower case.

### ad_auth::server::authorized::login

Active Directory group authorized to login on servers.

### ad_auth::server::authorized::sudo

Active Directory group authorized to sudo on servers.

### ad_auth::desktop::authorized::login

Active Directory group authorized to login on desktops.

### ad_auth::desktop::authorized::sudo

Active Directory group authorized to sudo on desktops.

### ad_auth::dyndns::interfaces

Default interfaces dynamically updated in Active Directory server's DNS.

## Usage

### authentification::default

Nothing special to signal, all attributes are optional and have a default value.

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[authentification]"
  ]
}
```

## License and Authors

Author: Hugo STEPHAN
Date: 2018


