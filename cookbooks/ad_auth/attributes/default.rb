#
# Cookbook Name:: ad_auth
# Attributes:: default
# Auuthor : Hugo STEPHAN
# Copyright : Mairie SMDF, 2018
#

## Default Access Group for server
default['ad_auth']['server']['authorized']['login'] = 'admins\ du\ domaine'
default['ad_auth']['server']['authorized']['sudo'] = ['admins\ du\ domaine']

## Default Access Group for desktop
default['ad_auth']['desktop']['authorized']['login'] = 'Utilisa.\ du\ domaine'
default['ad_auth']['desktop']['authorized']['sudo'] = ['admins\ du\ domaine']

## Default interfaces dynamically updated in AD
default['ad_auth']['dyndns']['interfaces'] = 'all'
