name             'ad_auth'
maintainer       'Mairie SMDF'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures Active Directory linux authentification'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
