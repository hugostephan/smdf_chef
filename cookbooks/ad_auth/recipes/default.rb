#
# Cookbook Name:: ad_auth
# Recipe:: default
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute
#

package 'sssd'
package 'samba-common'
package 'samba-common-tools'
package 'realmd'
package'krb5-workstation'
package 'oddjob'
package 'oddjob-mkhomedir'
package 'adcli'
package 'openldap-clients'
package 'policycoreutils-python'
package 'gvfs-smb'
package 'gvfs-fuse'

# Configuration Kerberos
template '/etc/krb5.conf' do
  source 'krb5.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

service 'sssd' do
end

# " || exit 0 " prevents script to crash chef-client in DMZ
execute 'Joining AD Domain' do
  command '(echo b7tLmPQTgm0oer1g9b9I | /usr/sbin/realm join --os-name=Linux --user=install saintmaur.local) || exit 0 ;'
end

# Configuration SSSD
template '/etc/sssd/sssd.conf' do
  source 'sssd.erb'
  owner 'root'
  group 'root'
  mode '0600'
end
