#
# Cookbook Name:: ad_auth
# Recipe:: serveur
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute
#

include_recipe '::default'

# Only admins can login on servers
# TO FIX : " || exit 0 " prevents script to crash chef-client in DMZ hosts
execute 'Restricting access to Admins only' do
  command "(realm permit -g #{node['ad_auth']['server']['authorized']['login']}@saintmaur.local ; systemctl restart sssd ;)  || exit 0"
end

# Adding servers' admin to sudoers
template '/etc/sudoers.d/admins' do
  source 'admin-server.erb'
  owner 'root'
  group 'root'
  mode '0440'
end
