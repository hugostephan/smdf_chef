#
# Cookbook Name:: ad_auth
# Recipe:: sso
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute
#

include_recipe '::default'

package 'cntlm'
package 'pam_script'

# Check for ntlm
execute 'Checking for pam custom script directory' do
  command 'mkdir -p /opt/smdf/pam/'
end

execute 'Checking for cntlm configuration directory' do
  command 'mkdir -p /etc/cntlm.d/ ; touch /etc/cntlm.d/smdf.conf ; chmod 777 -R /etc/cntlm.d/ ; chcon system_u:object_r:pam_var_run_t:s0 -R /etc/cntlm.d'
end

execute 'Configuring cntlm directory context' do
  command 'semanage fcontext -a -t pam_var_run_t /etc/cntlm.d/smdf.conf'
end

execute 'Restoring cntlm context' do
  command 'restorecon -v /etc/cntlm.d/smdf.conf'
end

execute 'Preparing custom selinux policy for cntlm' do
  command 'ausearch -c "pam_script_auth" --raw | audit2allow -M my-pamscriptauth'
  command 'setsebool polyinstantiation_enabled 1'
end


# SELinux 
template '/root/my-cntlm.te' do
  source 'my-cntlm.erb'
  owner 'root'
  group 'root'
  mode '0644'
end
template '/root/my-accountsdaemon.te' do
  source 'my-accountsdaemon.erb'
  owner 'root'
  group 'root'
  mode '0644'
end
template '/root/my-cp.te' do
  source 'my-cp.erb'
  owner 'root'
  group 'root'
  mode '0644'
end
template '/root/my-mkdir.te' do
  source 'my-mkdir.erb'
  owner 'root'
  group 'root'
  mode '0644'
end
template '/root/my-rm.te' do
  source 'my-rm.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

execute 'Preparing custom selinux policy for cntlm' do
  command 'checkmodule -M -m -o /root/my-cntlm.mod /root/my-cntlm.te ; semodule_package -o /root/my-cntlm.pp -m /root/my-cntlm.mod ; semodule -i /root/my-cntlm.pp'
end
execute 'Preparing custom selinux policy for account daemon' do
  command 'checkmodule -M -m -o /root/my-accountsdaemon.mod /root/my-accountsdaemon.te ; semodule_package -o /root/my-accountsdaemon.pp -m /root/my-accountsdaemon.mod ; semodule -i /root/my-accountsdaemon.pp'
end
execute 'Preparing custom selinux policy for copy' do
  command 'checkmodule -M -m -o /root/my-cp.mod /root/my-cp.te ; semodule_package -o /root/my-cp.pp -m /root/my-cp.mod ; semodule -i /root/my-cp.pp'
end
execute 'Preparing custom selinux policy for mkdir' do
  command 'checkmodule -M -m -o /root/my-mkdir.mod /root/my-mkdir.te ; semodule_package -o /root/my-mkdir.pp -m /root/my-mkdir.mod ; semodule -i /root/my-mkdir.pp'
end
execute 'Preparing custom selinux policy for remove' do
  command 'checkmodule -M -m -o /root/my-rm.mod /root/my-rm.te ; semodule_package -o /root/my-rm.pp -m /root/my-rm.mod ; semodule -i /root/my-rm.pp'
end

# Uploading homemade SSO script to store credentials once
template '/opt/smdf/pam/sso_ntlm' do
  source 'pam_sso_ntlm.erb'
  owner 'root'
  group 'root'
  mode '0777'
end

# Uploading pam_script triggered when authenticating
template '/etc/pam_script' do
  source 'pam_script.erb'
  owner 'root'
  group 'root'
  mode '0777'
end

# Environment variables to configure local proxy
template '/etc/environment' do
  source 'environment.erb'
  owner 'root'
  group 'root'
  mode  '0644'
end

# Handling PAM call to pam_script in standard auth system
execute 'Checking for system-auth configuration' do
	command 'sudo grep -F "pam_script" /etc/pam.d/system-auth || ( (echo "auth        optional      pam_script.so forward_pass expose=1" && cat /etc/pam.d/system-auth) > /tmp/pam.tmp && echo "y" | cp -f --remove-destination /tmp/pam.tmp /etc/pam.d/system-auth)'
end
execute 'Checking for sshd-auth configuration' do
	command 'sudo grep -F "pam_script" /etc/pam.d/sshd || ( (echo "auth        optional      pam_script.so forward_pass expose=1" && cat /etc/pam.d/sshd) > /tmp/pam.tmp && echo "y" | cp -f --remove-destination /tmp/pam.tmp /etc/pam.d/sshd)'
end
execute 'Checking for gdm-password configuration' do
	command 'sudo grep -F "pam_script" /etc/pam.d/gdm-password || ( (echo "auth        optional      pam_script.so forward_pass expose=1" && cat /etc/pam.d/gdm-password) > /tmp/pam.tmp && echo "y" | cp -f --remove-destination /tmp/pam.tmp /etc/pam.d/gdm-password)'
end
