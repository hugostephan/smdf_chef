#
# Cookbook Name:: ametys
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

users_manage 'ametys' do
  group_id 2301
  action [:remove, :create]
end