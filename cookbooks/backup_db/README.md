# Database Backup Cookbook

This cookbook allow to plan and execute database backup on a regular basis (at each chef execution).

## Requirements

This cookbook only requires linux architecture (mkdir is used to create backup folder).

### Platforms

- Generic Linux

### Chef

- Chef 12.0 or later

## Attributes

Attributes don't have default value, so they MUST be given in role.

### backup_db::backup_engine

Database engine used (mysql, pgsql, oracle, etc...).

### backup_db::backup_dbname

The name of the database to backup.

### backup_db::backup_output

The filename of the database backup output.

### backup_db::backup_options

Additional options for the backup job.

## Usage

### backup_db::default

Parameters must be given in role definition (except for backup_options).

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[authentification]"
  ]
}
```

## License and Authors

Author: Hugo STEPHAN
Date: 2018

