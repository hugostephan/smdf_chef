#
# Cookbook Name:: backup_db
# Recipe:: default
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute
#

# Params
engine  = node['backup_db']['backup_engine']
dbname  = node['backup_db']['backup_dbname']
output  = node['backup_db']['backup_output']
options = node['backup_db']['backup_options']


# Building db backup command 
cmd  = engine + ' ' + dbname + ' ' + options
cmd += ' > '
cmd += output + dbname + '-backup.sql'


# Making output dir if it doesn't exist
execute 'Making output dir if neeeded' do
	command 'mkdir -p ' + output
end


# Running db backup command
execute 'Backing up ' + dbname + ' database' do
	command cmd
end