default['ca-certificates']['package'] = "ca-certificates"
default['ca-certificates']['certificates_directory'] = "/etc/pki/ca-trust/source/anchors/"
default['ca-certificates']['databag_name'] = nil
default['ca-certificates']['update_command'] = "update-ca-trust force-enable; update-ca-trust extract"