name             'ca-certificates'
maintainer       'Mairie_Saint-Maur'
maintainer_email 'julien.huon@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures ca-certificates'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
