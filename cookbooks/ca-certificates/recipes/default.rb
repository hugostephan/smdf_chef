#
# Cookbook Name:: ca-certificates
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

# Installation du package
package node['ca-certificates']['package'] do
  action :upgrade
end


# Déploiement des certificats du databag
if node['ca-certificates']['databag_name']
  data_bag(node['ca-certificates']['databag_name']).each do |cert_name|
    certificate = data_bag_item(node['ca-certificates']['databag_name'], cert_name)	
	
    ca_certificates_certificate "#{node['ca-certificates']['certificates_directory']}/#{cert_name}.pem" do
	  content certificate['cert']
      owner "root"
      group "root"
      action :create
	  notifies :run, "execute[update-ca-certs]", :immediately
    end
  end
end


# Exécution de la commande update_ca_certs si un nouveau certificat a été déployé
execute 'update-ca-certs' do
  command node['ca-certificates']['update_command']
  action :nothing
end