# utils Cookbook

Gestion de la configuraiton spécifiques des machines de bureautique


## Requirements

Ce Cookbook n'est développé et testé que pour CentOS 7


### Platforms

- CentOS 7

### Chef

- Chef 12.0 or later

### Cookbooks

- 

## Attributes

Pas d'attributs particuliers

### desktop::default

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['desktop']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### desktop::default

TODO: Write usage instructions for each cookbook.

e.g.
Just include `desktop` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[desktop]"
  ]
}
```



Authors: Blaise

