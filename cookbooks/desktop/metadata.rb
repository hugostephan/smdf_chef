name             'desktop'
maintainer       'Mairie SMDF'
maintainer_email 'blaise.thauvin@mairie-saint-maur.com'
license          'GPL V2'
description      'Gestion de configuration des machines de bureautique'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
