#
# Cookbook Name:: desktop
# Recipe:: cups
#
# Copyright 2018, Ville de Saint-Maur des Fossés
#
# All rights reserved - Do Not Redistribute
#
# Installation des drivers d'impression et configuration des imprimantes 
#


# Ajouts des packages nécessaires 
package 'cups'
package 'system-config-printer'
package 'sane-backends'

#HP Attention, package ancien dans les repo. L'install auto en met un meilleur
# https://developers.hp.com/sites/default/files/hplip-3.17.11_rhel-7.0.x86_64.rpm
#package 'hplip-gui'


#Canon
# yum install libxml2 libxml2.i686 glibc glibc.i686 libstdc++ libstdc++.i686 libjpeg-turbo libjpeg-turbo.i686 beecrypt libglade2 
package 'libxml2'
package 'libxml2.i686'
package 'glibc'
package 'glibc.i686'
package 'libstdc++'
package 'libstdc++.i686'
package 'libjpeg-turbo'
package 'libjpeg-turbo.i686'
package 'beecrypt'
package 'libglade2'
package'libgcrypt-devel'
package'libgcrypt'

# Fichiers de configuration CUPS
template '/etc/cups/cupsd.conf' do
  source 'cups/cupsd.conf'
  owner 'root'
  group 'lp'
  mode '0640'
end

template '/etc/cups/printers.conf' do
  source 'cups/printers.conf'
  owner 'root'
  group 'lp'
  mode '0640'
end

template '/etc/cups/ppd/NB00001.ppd' do
  source 'cups/Canon-iR-ADV-4025-4035.ppd'
  owner 'root'
  group 'root'
  mode '0644'
end

template '/etc/cups/ppd/CO00001.ppd' do
  source 'cups/hp-color_laserjet_cm2320nf_mfp-pcl3.ppd'
  owner 'root'
  group 'root'
  mode '0644'
end



# Active l'agent CUPS et le redémarre en cas de modification de la configuration
service "cups" do
	action [ :start, :enable ]
	subscribes :restart, resources(:template => "/etc/cups/cupsd.conf", :template => "/etc/cups/printers.conf")
end