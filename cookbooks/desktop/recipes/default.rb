#
# Cookbook Name:: desktop
# Recipe:: default
#
# Copyright 2018, Ville de Saint-Maur des Fossés
#
# All rights reserved - Do Not Redistribute
#

# Si on est là c'est que le script de premier boot a fonctionné, on peut donc supprimer la ligne
fe = Chef::Util::FileEdit.new("/etc/rc.local")
fe.search_file_delete_line(/fisrtboot.sh/)
fe.write_file

# Ajouts des packages spécifiques aux machines de bureautique présents dans Epel
package 'saslwrapper'
package 'filezilla'
package 'libreoffice'
package 'libreoffice-langpack-fr'
package 'firefox'


##############################################################
## MAINTENANT GERE PAR UN AUTRE COOKBOOK : ad_auth::desktop ##
##############################################################
# Configuration SSSD
# template '/etc/sssd/sssd.conf' do
  # source 'sssd.erb'
  # owner 'root'
  # group 'root'
  # mode '0600'
  # notifies :restart, 'service[sssd]', :immediately
# end

# service 'sssd' do
# end

# Possibilité de se logger sans préciser le domaine
#fe = Chef::Util::FileEdit.new("/etc/sssd/sssd.conf")
#fe.search_file_replace_line(/use_fully_qualified_names/, "use_fully_qualified_names = False")
# Alerte expiration de mot de passe
#fe.insert_line_if_no_match(/pwd_expiration_warning/, "pwd_expiration_warning = 15")
#if fe.file_edited?
#   fe.write_file
#   service "sssd" do
#      action :restart
#   end
#end
#fe.write_file

# Ajout des admins dans la liste des sudoers
# template '/etc/sudoers.d/admins' do
  # source 'admins.erb'
  # owner 'root'
  # group 'root'
  # mode '0440'
# end


#Suppression de la liste des utilisateurs en page d'accueil
template '/etc/dconf/db/gdm.d/01-local-settings' do
  source '01-local-settings.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

# Ajout du script de login de montage des répertoires réseau
# Upload fusioninventory-agent configuration templates
template '/etc/profile.d/net-drives.sh' do
  source 'net-drives.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

# Modification du fichier de traductions de Nemo pour remplacer "dossier personnel" par "Dossier local"
# Modifié on-line sur https://localise.biz/free/poeditor
template '/usr/share/locale/fr/LC_MESSAGES/nemo.mo' do
  source 'nemo.mo'
  owner 'root'
  group 'root'
  mode '0644'
end

# configuration par défaut de Firefox
execute 'Creating firefox directory' do
	command 'mkdir -p /etc/skel/.mozilla/firefox/default/'
end

template '/etc/skel/.mozilla/firefox/default/prefs.js' do
  source 'prefs.js'
  owner 'root'
  group 'root'
  mode '0777'
end

template '/etc/skel/.mozilla/firefox/default/profiles.ini' do
  source 'profiles.ini.erb'
  owner 'root'
  group 'root'
  mode '0777'
end

execute 'Configuring firefox browser' do
  command 'cp -rf /etc/skel/.mozilla/firefox/default/prefs.js /usr/lib64/firefox/browser/defaults/preferences/all-smdf.js ; rm -rf /usr/lib64/firefox/browser/defaults/preferences/all-redhat.js'
end
