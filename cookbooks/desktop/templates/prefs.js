// Language
pref("browser.search.countryCode", "FR");
pref("browser.search.region", "FR");

// Homepage
pref("browser.urlbar.autoFill", false);
pref("browser.startup.homepage", "http://intranet.mairie-saint-maur.com/fr/index.html");
pref("startup.homepage_override_url", "http://intranet.mairie-saint-maur.com/fr/index.html");
pref("startup.homepage_welcome_url", "http://intranet.mairie-saint-maur.com/fr/index.html");

// Proxy
pref("network.proxy.type", 1);
pref("network.proxy.http", "localhost");
pref("network.proxy.http_port", 3128);
pref("network.proxy.ssl", "localhost");
pref("network.proxy.ssl_port", 3128);

// SSO SPNEGO
pref("network.negotiate-auth.allow-non-fqdn",true);
pref("network.negotiate-auth.trusted-uris", "sso.mairie-saint-maur.com, sso-x.mairie-saint-maur.com, intranet-x.mairie-saint-maur.com, cms-intranet-x.mairie-saint-maur.com, srv-eon.saintmaur.local, weathermap.saintmaur.local");