# DNS Configuration Cookbook

This cookbook installs and configures DNS using dnsmasq.

## Requirements

This cookbook only requires linux host architecture with standard repositories configured and a working internet connection.

### Platforms

- CentOS (or generic linux)

### Chef

- Chef 12.0 or later

## Attributes

### dns_config::zone

DNS zone to use :
- dmz   : 192.168.254.0/24
- rpriv : 172.24.0.0/16

## Usage

### dns_config::default

Nothing special to signal, all attributes are optional and have a default value.

```json
{
  "name":"my_node",
  "zone": "dmz"|"rpriv"
  "run_list": [
    "recipe[dns_config]"
  ]
}
```

## License and Authors

Author: Hugo STEPHAN
Date: 2018
