name             'dns_config'
maintainer       'Mairie Saint-Maur-des-Fossés'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Configures DNS : /etc/resolv.conf'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends			 'network_manager'
