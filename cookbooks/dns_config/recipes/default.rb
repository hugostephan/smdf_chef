#
# Cookbook Name:: dns_config
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# including network manager recipe to keep NM from overwritting DNS config
include_recipe 'network_manager'

template '/etc/resolv.conf' do
  source 'resolv.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

service "dnsmasq" do
  action :restart
end