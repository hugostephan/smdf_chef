case node.chef_environment
  when 'development'
    default['esabora']['databases'] = [
      "esabora_dev"
    ]
  when 'integration'
    default['esabora']['databases'] = [
      "esabora_int"
    ]
  when 'preproduction'
    default['esabora']['databases'] = [
      "esabora_preprod"
    ]
  when 'production'
    default['esabora']['databases'] = [
      "esabora_prod"
    ]
end

default['esabora']['mysql_users_databag'] = nil