#
# Cookbook Name:: fstab
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Upload script to clear fstab
template '/root/clear_fstab' do
  mode 0744
  owner 'root'
  group 'root'
end

# Upload script to condtionally add line in fstab
template '/root/add_line' do
  mode 0744
  owner 'root'
  group 'root'
end

# Saving old fstab (debug purpose)
execute 'Saving old fstab /etc/fstab.old' do
  command 'cp -f /etc/fstab /etc/fstab.old'
end

# Working on new fstab
execute 'Initializing fstab.new buffer file' do
  command 'cp -f /etc/fstab /etc/fstab.new'
end

# Clearing fstab
execute 'Clearing existing fstab entries' do
  command '/root/clear_fstab'
end

# Adding specific line if there are not already in file
cpt = 0
node['fstab']['partitions'].each do |part, opt|
  execute 'Adding fstab line ' + cpt.to_s do
    command "/root/add_line '" + part + "' '" + opt + "'"
  end
  cpt += 1
end

# If everything went fine copying fstab.new to fstab
execute 'Activating new fstab file' do
  command 'cp -f /etc/fstab.new /etc/fstab'
end
execute 'Deleting fstab.new buffer file' do
  command 'rm -rf /etc/fstab.new'
end

# Mounting all fstab content
node['fstab']['partitions'].each do |part, _opt|
  p = part.split(' ')[0]
  execute '(Re)Mounting ' + p + ' partition'  do
    command 'umount ' + p + ' || true ; mount ' + p + ' || true'
  end
end
