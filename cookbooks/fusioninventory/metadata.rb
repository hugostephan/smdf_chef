name             'fusioninventory'
maintainer       'Mairie SMDF'
maintainer_email 'blaise.thauvin@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures fusioninventory'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
