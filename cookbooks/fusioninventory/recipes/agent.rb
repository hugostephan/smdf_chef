#
# Cookbook Name:: fusioninventory
# Recipe:: agent
#
# Copyright 2018, Ville de Saint-Maur des Fossés
#
# All rights reserved - Do Not Redistribute
#

package 'fusioninventory-agent'


# Upload fusioninventory-agent configuration templates
template '/etc/fusioninventory/agent.cfg' do
  source 'agent.cfg.erb'
  owner 'root'
  group 'root'
  mode '0644'
end
template '/etc/sysconfig/fusioninventory-agent' do
  source 'fusioninventory-agent.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

# Active l'agent et le redémarre en cas de modification de la configuration
service "fusioninventory-agent" do
	action [ :start, :enable ]
	subscribes :restart, resources(:template => "/etc/fusioninventory/agent.cfg")
end
