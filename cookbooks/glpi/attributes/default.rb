case node.chef_environment
  when 'development'
    default['glpi']['databases'] = [
      "glpi_dev"
    ]
  when 'integration'
    default['glpi']['databases'] = [
      "gpli_int"
    ]
  when 'preproduction'
    default['glpi']['databases'] = [
      "glpi_preprod"
    ]
  when 'production'
    default['glpi']['databases'] = [
      "glpi_prod"
    ]
end

default['glpi']['mysql_users_databag'] = nil