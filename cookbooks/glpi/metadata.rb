name             'glpi'
maintainer       'Mairie_Saint-Maur'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures smdf-applications'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'mysql', '>= 8.0.1'
depends 'mysql_prov', '>= 0.1.0'
depends 'yum-mysql-community', '>= 0.3.0'
depends 'selinux_policy', '>= 0.9.5'