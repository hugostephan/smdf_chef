jetty CHANGELOG
===============

0.1.2
-----
- [jhuon] - Ajout de la possibilité de cacher la version de jetty (balise server)

0.1.1
-----
- [jhuon] - Amélioration de la création du lien symbolique jetty_home

0.1.0
-----
- [jhuon] - Initial release of jetty

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
