default['jetty']['install_java'] = 'true'
default['jetty']['tgz_mirror'] = 'http://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/9.3.11.v20160721/'
default['jetty']['tgz_name'] = 'jetty-distribution-9.3.11.v20160721.tar.gz'
default['jetty']['tgz_checksum'] = '6f720a39324ba02491c5dd598039f9cda1746d45c26594f8189692058f9f4264'
default['jetty']['user'] = 'jetty'
default['jetty']['group'] = 'jetty'
default['jetty']['install_dir'] = '/opt/jetty/'
default['jetty']['jetty_home'] = '/opt/jetty/currentversion'
default['jetty']['instances_dir'] = '/opt/jetty/instances'

default['jetty']['create_base_instance'] = true
default['jetty']['base_http_port'] = 8080
default['jetty']['base_jetty_user'] = "jetty"
default['jetty']['base_jetty_group'] = "jetty"
default['jetty']['base_java_options'] = "-server -Xms128m -Xmx256m -XX:+DisableExplicitGC"
default['jetty']['base_jetty_hide_version'] = nil

default['jetty']['instances'] = {
}
