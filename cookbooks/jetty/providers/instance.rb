#
# Cookbook Name:: jetty
# Provider:: instance
#

action :configure do
  # On créer le dossier qui va contenir les instances
  directory node['jetty']['instances_dir'] do
    mode '0755'
  end

  # Création de l'utilisateur/group
  user "#{new_resource.jetty_user}" do
    system true
    shell "/bin/bash"
    home "#{node['jetty']['instances_dir']}/#{new_resource.name}/"
    action :create
  end

  group "#{new_resource.jetty_group}" do
    system true
    action :create
  end

  # On créer l'instance 
  directory "#{node['jetty']['instances_dir']}/#{new_resource.name}" do
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  directory "#{node['jetty']['instances_dir']}/#{new_resource.name}/etc" do
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  directory "#{node['jetty']['instances_dir']}/#{new_resource.name}/webapps" do
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  directory "#{node['jetty']['instances_dir']}/#{new_resource.name}/logs" do
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  # Déploiement des configurations
  template "#{node['jetty']['instances_dir']}/#{new_resource.name}/etc/jetty.xml" do
    source "jetty.xml.erb"
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  template "#{node['jetty']['instances_dir']}/#{new_resource.name}/etc/jetty-http.xml" do
    source "jetty-http.xml.erb"
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
    mode '0755'
  end

  template "#{node['jetty']['instances_dir']}/#{new_resource.name}/etc/jetty-deploy.xml" do
    source "jetty-deploy.xml.erb"
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  template "#{node['jetty']['instances_dir']}/#{new_resource.name}/etc/jetty-logging.xml" do
    source "jetty-logging.xml.erb"
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
  end

  template "#{node['jetty']['instances_dir']}/#{new_resource.name}/start.ini" do
    source "start.ini.erb"
    mode '0755'
    owner "#{new_resource.jetty_user}"
    group "#{new_resource.jetty_group}"
    variables(
      :jetty_hide_version => new_resource.jetty_hide_version
    )
  end

  # Création du script d'init de l'instance
  service_name = "jetty"
  if new_resource.name != "base" 
    service_name = "jetty-" + new_resource.name 
  end
 
  case node['platform_version'].to_i
  when 6
    execute "create_init_script" do
      command "ln -f -s #{node['jetty']['jetty_home']}/bin/jetty.sh /etc/init.d/#{service_name}"
      cwd "/"
      user "root"
      group "root"
    end
	
	template "/etc/default/#{service_name}" do
      source 'default.erb' 
      mode '0755'
      variables(
        :port => "#{new_resource.http_port}",
        :java_options => "#{new_resource.java_options}",
        :jetty_base => "#{node['jetty']['instances_dir']}/#{new_resource.name}/",
        :user => "#{new_resource.jetty_user}",
        :group => "#{new_resource.jetty_group}"
      )
    end
  when 7
	template "/etc/systemd/system/#{service_name}.service" do
      source 'systemd.service.erb' 
      mode '0644'
      variables(
	    :port => "#{new_resource.http_port}",
        :java_options => "#{new_resource.java_options}",
        :jetty_base => "#{node['jetty']['instances_dir']}/#{new_resource.name}/",
        :user => "#{new_resource.jetty_user}",
        :group => "#{new_resource.jetty_group}",
		:pid_file => "#{node['jetty']['instances_dir']}/#{new_resource.name}/jetty.pid"
      )
	  notifies :run, 'execute[reload_systemd]', :immediately
    end
	
	execute "reload_systemd" do
      command "systemctl daemon-reload"
      cwd "/tmp"
      action :nothing
    end
	
	template "#{node['jetty']['instances_dir']}/#{new_resource.name}/.jettyrc" do
      source 'default.erb' 
      mode '0755'
      variables(
        :port => "#{new_resource.http_port}",
        :java_options => "#{new_resource.java_options}",
        :jetty_base => "#{node['jetty']['instances_dir']}/#{new_resource.name}/",
        :user => "#{new_resource.jetty_user}",
        :group => "#{new_resource.jetty_group}"
      )
	  user new_resource.jetty_user
      group new_resource.jetty_group
    end
	
  end
  
  # On démarre l'instance
    service "#{service_name}" do
      action [:start, :enable]
      notifies :run, "execute[wait for #{service_name}]", :immediately
      retries 4
      retry_delay 30
    end

    execute "wait for #{service_name}" do
      command 'sleep 5'
      action :nothing
    end
end
