#
# Cookbook Name:: jetty
# Recipe:: default
#
# Copyright 2015, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

# Pré-requis : JDK
include_recipe 'java' if node['jetty']['install_java']

# Création du Dossier Jetty
directory node['jetty']['install_dir'] do
  owner 'root'
  group 'root'
  mode 0755
  action :create
end

# Récupération du ZIP d'installation
remote_file "#{Chef::Config[:file_cache_path]}/#{node['jetty']['tgz_name']}" do
  source "#{node['jetty']['tgz_mirror']}#{node['jetty']['tgz_name']}"
  action :create
  mode "0755"
  owner "root"
  group "root"
  checksum node['jetty']['tgz_checksum']
  notifies :run, 'execute[extract_jetty_tgz]', :immediately
end

# Extraction du TGZ d'installation
execute "extract_jetty_tgz" do
  command "tar -xzf #{node['jetty']['tgz_name']} -C #{node['jetty']['install_dir']}"
  cwd "#{Chef::Config[:file_cache_path]}"
  user "root"
  group "root"
  action :nothing
  notifies :create, "link[#{node['jetty']['jetty_home']}]", :immediately
end

# Création du lien symbolique jetty_home
link "#{node['jetty']['jetty_home']}" do
  to "#{node['jetty']['install_dir']}#{node['jetty']['tgz_name'].chomp(".tar.gz")}"
  link_type :symbolic
  action :nothing
end

# Création du/des instances définies dans le fichier attributes

if node['jetty']['create_base_instance']
  jetty_instance 'base' do
    http_port node['jetty']['base_http_port']
    java_options node['jetty']['base_java_options']
    jetty_user node['jetty']['base_jetty_user']
    jetty_group node['jetty']['base_jetty_group']
    jetty_hide_version node['jetty']['base_jetty_hide_version']
    action :configure
  end
end

node['jetty']['instances'].each do |name, attrs|
  jetty_instance "#{name}" do
    http_port attrs['http_port']
    java_options attrs['java_options']
    jetty_user attrs['jetty_user']
    jetty_group attrs['jetty_group']
    jetty_hide_version attrs['jetty_hide_version']
    action :configure
  end
end
