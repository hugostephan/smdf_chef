actions :configure

default_action :configure

attribute :http_port,
  :kind_of => Fixnum

attribute :jetty_user,
  :kind_of => String

attribute :jetty_group,
  :kind_of => String

attribute :java_options,
  :kind_of => String

attribute :jetty_hide_version,
  :kind_of => String
