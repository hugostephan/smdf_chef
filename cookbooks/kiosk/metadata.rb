name             'kiosk'
maintainer       'Mairie_Saint-Maur'
maintainer_email 'olivier.aubert@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures kiosk'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
