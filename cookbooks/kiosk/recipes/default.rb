#
# Cookbook Name:: kiosk
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

template '/home/pi/.config/lxsession/LXDE-pi/autostart' do
  mode 0644
  owner 'pi'
  group 'pi'
  source 'autostart.erb'
end