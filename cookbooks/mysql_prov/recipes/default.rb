#
# Cookbook Name:: mysql_prov
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute

mysql2_chef_gem 'default' do
  gem_version '0.3.17'
  client_version '5.7'
  action :install
end