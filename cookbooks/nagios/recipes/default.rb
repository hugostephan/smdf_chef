#
# Cookbook Name:: eyesofnetwork
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Running nagios configuration recipes in correct order
include_recipe 'nagios::notifications'
include_recipe 'nagios::resources'
include_recipe 'nagios::export'

