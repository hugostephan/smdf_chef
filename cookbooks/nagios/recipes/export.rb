#
# Cookbook Name:: nagios
# Recipe:: export
#
# Copyright 2017, SMDF
#
# All rights reserved - Do Not Redistribute

# Upload lilac->nagios export script
template '/root/export-config' do
	mode 0744
	owner 'root'
	group 'root'
end

# Running export script
execute 'Running lilac to nagios export script' do
	command '/root/export-config'
end