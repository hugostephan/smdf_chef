#
# Cookbook Name:: nagios
# Recipe:: notifications
#
# Copyright 2017, SMDF
#
# All rights reserved - Do Not Redistribute

# Initialize query
query = "USE lilac ; UPDATE nagios_contact SET email = '" + node['nagios']['notification_email'] + "' "

# Building up query from role's data
node['nagios']['notification_parameters'].each do |name,value|
	if value == true
		value=1
	elsif value == false
		value=0
	end
	query += ", " + name + " = " + value.to_s + " "
end

# Finishing query
query += " WHERE name = '" + node['nagios']['notification_user'] + "' "

# Run mysql query
execute 'Running nagios notifications configuration query' do
	command 'mysql -e "' + query + '"'
end