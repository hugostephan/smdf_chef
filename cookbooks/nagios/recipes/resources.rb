#
# Cookbook Name:: nagios
# Recipe:: resources
#
# Copyright 2017, SMDF
#
# All rights reserved - Do Not Redistribute

# Initialize query
query = "USE lilac ; TRUNCATE nagios_resource ; INSERT INTO nagios_resource VALUES (NULL "
i = 1

# Building query from roles data
node['nagios']['resources'].each do |name,value|
	query = query + " , '" + value + "'"
	i += 1
end

# We need to fill all nagios' 32 variables 
while i < 33
	query = query + ", NULL "
	i += 1
end

# Finishing query
query = query + ")"

# Run mysql query
execute 'Running nagios resources insert query' do
	command 'mysql -e "' + query + '"'
end