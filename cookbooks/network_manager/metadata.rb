name             'network_manager'
maintainer       'Mairie Saint-Maur-des-Fossés'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Configures network manager : /etc/NetworkManager/NetworkManager.conf'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
