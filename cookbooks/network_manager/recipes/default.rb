#
# Cookbook Name:: network_manager
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

template '/etc/NetworkManager/NetworkManager.conf' do
  source 'network_manager.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

service "NetworkManager" do
  action :restart
end