#
# Cookbook Name:: notif_sms
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Upldating notifier_sms hostlist config
template '/srv/eyesofnetwork/notifier_sms/conf/hostlist' do
  source 'hostlist.erb'
  owner 'root'
  group 'root'
  mode '0666'
end

# Upldating notifier_sms statuslist config
template '/srv/eyesofnetwork/notifier_sms/conf/statuslist' do
  source 'statuslist.erb'
  owner 'root'
  group 'root'
  mode '0666'
end

# Upldating notifier_sms recipients config
template '/srv/eyesofnetwork/notifier_sms/conf/numbers' do
  source 'numbers.erb'
  owner 'root'
  group 'root'
  mode '0666'
end

if node['notif_sms']['sms_notification_enabled'] == false
	execute 'Disable SMS host notification' do
		command 'mv /srv/eyesofnetwork/notifier_sms/conf/hostlist /srv/eyesofnetwork/notifier_sms/conf/hostlist.old ; touch /srv/eyesofnetwork/notifier_sms/conf/hostlist'
	end
	execute 'Disable SMS service notification' do
		command 'mv /srv/eyesofnetwork/notifier_sms/conf/statuslist /srv/eyesofnetwork/notifier_sms/conf/statuslist.old ; touch /srv/eyesofnetwork/notifier_sms/conf/statuslist ;' 
	end
else
	execute 'Remove temp old file' do
		command 'rm -rf /srv/eyesofnetwork/notifier_sms/conf/hostlist.old /srv/eyesofnetwork/notifier_sms/conf/statuslist.old'
	end
end