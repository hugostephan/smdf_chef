# Oracle JDK + JCE
default['java']['jdk_version'] = '8'
default['java']['install_flavor'] = 'oracle'
default['java']['set_etc_environment'] = true
default['java']['oracle']['accept_oracle_download_terms'] = true
default['java']['oracle']['jce']['enabled'] = true

# OpenIDM
default['openidm']['zip_repository'] = "http://download.forgerock.org/downloads/openidm/snapshot/"
default['openidm']['version'] = "4.0.0-SNAPSHOT"
default['openidm']['checksum'] = '786c6e307320df8eb06819d76e327f2ef3c5f4df7e5a6e038e8c0c07ff88b063'

default['openidm']['user'] = "openidm"
default['openidm']['group'] = "openidm"
default['openidm']['home_dir'] = "/opt/openidm"