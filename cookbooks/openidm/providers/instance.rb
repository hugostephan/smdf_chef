#
# Cookbook Name:: openidm
# Provider:: instance
#

use_inline_resources

action :install do
  # User and Group creation
  group new_resource.group do
    system true
    action :create
  end

  user new_resource.user do
    system true
    shell "/bin/bash"
    group new_resource.group
    action :create
  end
    
  directory "#{new_resource.name}" do
    owner new_resource.user
    group new_resource.group
	action :create
  end
  
  # Download ZIP
  zip_name = "openidm-#{new_resource.version}.zip"
  src_dir = "#{Chef::Config[:file_cache_path]}/openidm-#{new_resource.version}"
  
  remote_file "#{Chef::Config[:file_cache_path]}/#{zip_name}" do
    source "#{new_resource.repository}/#{zip_name}"
    action :create
    mode "0755"
    owner "root"
    group "root"
    checksum new_resource.checksum
    notifies :run, 'execute[extract_openidm_zip]', :immediately
  end
  
  # Extract ZIP
  execute "extract_openidm_zip" do
    command "unzip #{zip_name} -d #{new_resource.name}/.."
    cwd Chef::Config[:file_cache_path]
    user "root"
    group "root"
    action :nothing
	not_if { ::Dir.exist?( new_resource.name + '/bin' ) }
  end
  
  # Change openidm files owner
  ::Dir.glob("#{new_resource.name}/**/*").each do |path|
    directory path do 
      owner new_resource.user
      group new_resource.group
    end
  end
end