#
# Cookbook Name:: openidm
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

# Requirements
[ 'java'].each do |rec|
  include_recipe rec
end

# Installation
openidm_instance node['openidm']['home_dir'] do
  user node['openidm']['user']
  group node['openidm']['group']
  repository node['openidm']['zip_repository'] 
  version node['openidm']['version']
  checksum node['openidm']['checksum']
  action [:install]
end

if (node['idm'] == 'new')

	# Upload mysql connector
	cookbook_file '/opt/openidm/bundle/mysql-connector-java-5.1.40-bin.jar' do
		source 'mysql-connector-java-5.1.40-bin.jar'
		owner 'openidm'
		group 'openidm'
		mode '0644'
		action :create
	end

	# Copying conf files
	execute 'delete_repo_orientdb' do
		command 'rm -rf /opt/openidm/conf/repo.orientdb.json || true'
	end
	execute 'copy_datasource_jdbc' do
		command 'cp /opt/openidm/db/mysql/conf/datasource.jdbc-default.json /opt/openidm/conf/datasource.jdbc-default.json'
	end
	execute 'copy_repo_jdbc' do
		command 'cp /opt/openidm/db/mysql/conf/repo.jdbc.json /opt/openidm/conf/repo.jdbc.json'
	end

	# Getting mysql db user informations
	db = data_bag_item('openidm_mysql_users_new', node.chef_environment)
	name = ""
	passwd = ""
	users = db['users']
	users.each do |key,values|
		name = key
		passwd = values['password']
	end

	# To configure jdbc datasource connection
	template '/opt/openidm/conf/datasource.jdbc-default.json' do 
		source 'datasource.jdbc.erb'
		action :create
		variables ({ 
			:name => "#{name}",
			:passwd => "#{passwd}"
		})
	end

	# Enable openidm service
	template '/etc/systemd/system/openidm.service' do
		source 'openidm.service.erb'
		action :create
	end
	
	# Configure firewall to open port 8080 and 8443
	execute 'firewalld add port 8080 permanently' do
		command 'firewall-cmd --zone=public --add-port=8080/tcp --permanent'
	end
	execute 'firewalld add port 8443 permanently' do
		command 'firewall-cmd --zone=public --add-port=8443/tcp --permanent'
	end
	
	execute 'firewalld add port 161 permanently' do
		command 'firewall-cmd --zone=public --add-port=161/udp --permanent'
	end
	execute 'firewalld add port 162 permanently' do
		command 'firewall-cmd --zone=public --add-port=162/udp --permanent'
	end
	
	execute 'restart firewalld' do
		command 'systemctl restart firewalld'
	end
	
end