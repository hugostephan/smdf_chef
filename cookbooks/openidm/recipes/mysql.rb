#
# Cookbook Name:: openidm
# Recipe:: openidm_mysql
#
# Copyright 2015, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute

# Pré-requis : Installation de MySQL
include_recipe 'yum-mysql-community::mysql57'
include_recipe 'mysql_prov'

# Options MySQL
options_mysql = {
  "sql_mode" => "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION",
  "character-set-server" => "utf8",
  "collation-server" => "utf8_general_ci",
  "expire_logs_days" => "30",
  "lower_case_table_names" => "1"
}

mysql_service 'default' do
  version '5.7'
  bind_address '0.0.0.0'
  port '3306'
  action :create
  mysqld_options options_mysql
  notifies :run, "bash[selinux_set_contexts]", :immediately
end

# On donne les bons contextes SELinux
bash "selinux_set_contexts" do
  cwd "/tmp"
  code <<-EOH
    chcon -u system_u -r object_r -t mysqld_etc_t /etc/mysql-default -R
	chcon -u system_u -r object_r -t mysqld_log_t /var/log/mysql-default -R
	chcon -u system_u -r object_r -t mysqld_var_run_t /var/run/mysql-default -R
	chcon -u system_u -r object_r -t mysqld_db_t /var/lib/mysql-default -R
	chcon -u system_u -r object_r -t mysqld_db_t /var/lib/mysql-keyring -R
	chcon -u system_u -r object_r -t mysqld_db_t /var/lib/mysql-files -R
  EOH
  action :nothing
  notifies :start, "mysql_service[default]", :immediately
end

# Set Root Password
root_password = (0...12).map { ('a'..'z').to_a[rand(26)] }.join # generate root password
execute "mysql-set-root-password" do
  command "mysqladmin -S /var/run/mysql-default/mysqld.sock -u root -pilikerandompasswords password #{root_password}"
  not_if {File.exists?("/root/.my.cnf")}
end

template "/root/.my.cnf" do
  action :create_if_missing
  source "my.cnf.client.erb"
  owner "root"
  group "root"
  mode  0600
  variables({
    :password => root_password,
	:socket => "/var/run/mysql-default/mysqld.sock"
  })
end

# Création de la base de données
mysql_database node['openidm']['mysql_database'] do
  action :create
  socket '/var/run/mysql-default/mysqld.sock'
end

mysql_users_databag = node['openidm']['mysql_users_databag']

# Création des utilisateurs MySQL
data_bag_item(mysql_users_databag,node.chef_environment)['users'].each do |mysql_user,mysql_user_properties|
  mysql_database_user mysql_user do
    host mysql_user_properties['host']
    password mysql_user_properties['password']
    privileges ['all privileges']
	socket '/var/run/mysql-default/mysqld.sock'
	database_name node['openidm']['mysql_database']
    action [:create, :grant]
  end
end
