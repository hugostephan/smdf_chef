actions :install
default_action :install

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true
  
attribute :user,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true

attribute :repository,
  :kind_of => String,
  :required => true

attribute :version,
  :kind_of => String,
  :required => true

attribute :checksum,
  :kind_of => String,
  :required => true