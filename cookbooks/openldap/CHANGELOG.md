openldap CHANGELOG
==================

1.0.5
-----
- [huon-jul] - Ajout auth_method dans delegate

1.0.4
-----
- [huon-jul] - Correction attributes -> syslog uniquement si server

1.0.3
-----
- [huon-jul] - Correction recette client -> check certificat importé

1.0.2
-----
- [huon-jul] - Correction recette client -> Chemin certificat

1.0.1
-----
- [huon-jul] - Correction : On regénère cn=config si le schéma change

1.0.0
-----
- [huon-jul] - Finalisation du cookbook et de sa documentation

0.1.0
-----
- [huon-jul] - Initial release of OpenLDAP
