openldap Cookbook
=================

Ce cookbook propose deux recettes :

- default : Permet d'installer les clients OpenLDAP (ldapsearch, ldapmodify, ldapadd...) et de configurer le fichier ldap.conf
- server : Permet d'installer et de configurer un serveur OpenLDAP qui peut faire partie d'une réplication.

Requirements
------------
#### platforms
- Centos 6
- Centos 7

#### Cookbook
- yum-ina (recette server)

#### Gem
- deep_merge (recette server)

Attributes
----------
#### openldap::default

Attribute | Description | Type | Default|Required
----------|-------------|------|--------|--------
node['openldap']['clients']['ldap.conf']|Emplacement du fichier ldap.conf|String|/etc/openldap/ldap.conf|TRUE
node['openldap']['clients']['cacert_dir']|Emplacement de la base de données certutil dans laquelle importer le(s) certificat(s) pour l'accès ldaps ou ldap + STARTTLS|String|/etc/openldap/certs|TRUE
node['openldap']['clients']['uri']|URL d'accès a(ux) serveur(s) LDAP|String||FALSE
node['openldap']['clients']['base']|BaseDN de recherche|String||FALSE
node['openldap']['clients']['sizelimit']|Nombre d'entrée maximum retournées par une recherche|Fixnum||FALSE
node['openldap']['clients']['timelimit']|Temps maximum pris par une recherce|Fixnum||FALSE
node['openldap']['clients']['deref']|Alias Dereferencing|String|never|FALSE
node['openldap']['clients']['certificates']|Certificat(s) (clé(s) publique(s)) à importer dans la base certutil|Hash|FALSE

Vous pouvez consulter [l'aide de ldap.conf](http://linux.die.net/man/5/ldap.conf) pour mieux comprendre comment renseigner les paramètres ci-dessus.

#### openldap::server

Attribute | Description | Type | Default
----------|-------------|------|--------
node['openldap']['server']|Active la recette server|Boolean|FALSE
node['openldap']['databag']|Nom du databag contenant un item par environnement|String|
node['openldap']['user']|Utilisateur système démarrant OpenLDAP|String|ldap
node['openldap']['group']|Groupe système démarrant OpenLDAP|String|ldap
node['openldap']['ip']|IP sur laquelle/lesquelles écouter|String|*
node['openldap']['ssl_ip']|IP sur laquelle/lesquelles écouter pour le ldaps|String|*
node['openldap']['port']|Port sur lequel écouter pour le ldap|String|389
node['openldap']['ssl_port']|Port sur lequel écouter pour le ldaps|String|636
node['openldap']['loglevel']|Loglevel du serveur|String|sync stats
node['openldap']['limits']|Hash contenant les limites pour sizelimit, timelimit et idletiemout|Hash|
node['openldap']['tls']|Hash contenant les configurations TLS|Hash|
node['openldap']['schemas']|Fichier de schémas à utiliser sur l'annuaire|Array|
node['openldap']['external_schemas]|Fichier de schémas supplémentaires à récupérer par HTTP|Hash|
node['openldap']['delegate']|Active/Désactive la délégation d'authentification à un autre annuaire|Boolean|false
node['openldap']['delegate_uri']|URL du/des LDAP vers lequels déléguer l'authentification|String|
node['openldap']['delegate_base']|BaseDN dans lequel rechercher les utilisateurs sur l'annuaire distant|String|
node['openldap']['delegate_filter']|Filtre permettant de rechercher l'utilisateur sur l'annuaire distant|String|
node['openldap']['id']|Dans le cas de réplication, tous les maitres doivent être listés dans ce Hash avec un ID Unique|Hash
node['openldap']['suffixes']|Hash contenant tous les suffixes à créer sur l'annuaire et leurs configurations|Hash|
node['openldap']['replications']|Hash contenant toutes les réplications|Hash|
node['openldap']['ldif_to_deploy']|Hash contenant les URLS de fichiers LDIF à importer sur l'annuaire|


Descriptions des valeurs attendues :

- node['openldap']['limits']

Attribute | Description | Type | Default
----------|-------------|------|--------
sizelimit|Limites (globale ou par utilisateur) d'entrées retournées par recherche|Array|['size.soft=5000 size.hard=5000 size.pr=5000 size.prtotal=unlimited']
timelimit|Temps maximum pris par une recherce|String|3600
idletimeout|Temps d'inactivité maximum d'une connexion|String|10800

- node['openldap']['tls']

Attribute | Description | Type | Default
----------|-------------|------|--------
enabled|Activation/Désactivation du TLS (LDAPS ou STARTTLS)|Boolean|false
directory|Dossier dans lequel déposer le certificat et sa clé privée|String|/usr/local/openldap/etc/openldap/tls
TLSCACertificateFile|Nom du fichier contenant le certificat de l'autorité ayant émis le certificat|String|openldapCA.pem
TLSCertificateFile|Nom du fichier contenant le certificat|String|openldap.pem
TLSCertificateKeyFile|Nom du fichier contenant la clé privée du certificat|String|openldap.key
TLSProtocolMin|Version minim de SSL/TLS|String|3.1 (TLS 1.0)
TLSCipherSuite|Ciphers à utiliser|String|ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSAAES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA

- node['openldap']['external_schemas] 

Il doit un item par schéma à récupérer avec "nom_fichier": "URL où le récupérer".

```json
{
  "openldap": {
    "external_schemas": {
      "ina.schema": "http://src.infra.sas.ina/openldap/ina.schema"
    }
  }
}
```

- node['openldap']['id']

Il doit contenir un item par serveur avec son ID unique (l'ID unique n'est nécessaire que pour un MASTER).

```json
{
  "openldap": {
    "id": {
      "ldap01.infra.i.priv.ina": 1
    }
  }
}
```

- node['openldap']['suffixes']

Il doit contenir toutes les [configurations](http://www.openldap.org/software/man.cgi?query=slapd.conf&apropos=0&sektion=0&manpath=OpenLDAP+2.4-Release&format=html) pour un suffixe donné.

Exemple pour créer un suffixe o=ina avec un backend de type mdb :

```json
{
  "openldap": {
    "suffixes": {
      "o=ina": {
        "database": "mdb",
        "rootdn": "cn=Manager,o=ina",
        "suffix": "o=ina",
        "directory": "/usr/local/openldap/var/openldap-data/ina",
        "rootpw": "pouet"
      }
    }
  }
}
```

Ces configurations peuvent également être contenues dans un databag chiffré avec un item par environnement.


- node['openldap']['replications']

Va contenir les configurations de réplications, si réplication, pour chaque suffixe répliqué et pour chaque serveur.

Pour chaque serveur on doit avoir le DN compte du compte de réplication, son mot de passe et enventuellement les groupes auquels il doit appartenir.

Exemple :

```json
"replications": {
  "o=ina": {
    "ldap01.infra.p.sas.ina": {
      "replication_account": "cn=replicator-ldap01.infra.p.sas.ina,ou=Technical Accounts,o=ina",
      "replication_password": "PouetPouet",
      "groups": [
        "cn=Read-Only Administrators,ou=Groups,o=ina",
        "cn=Read-Only Passwords Administrators,ou=Groups,o=ina"
       ]
    }
  }
} 
```

Ces configurations peuvent également être contenues dans un databag chiffré avec un item par environnement.


- node['openldap']['databag']

Des configurations peuvent être contenues dans un databag chiffré. Il doit il avoir un item par environnement. Chaque item peut contenir les configuations de suffixes, les certificats et la réplication.

```json
{
  "id": "integration",
  "tls": {
    "openldap.pem": "...",
    "openldap.key": "...",
    "openldapCA.pem: "..."
  },
  "suffixes": {},
  "replications": {}
}
```

Usage
-----
#### openldap::default

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[openldap]"
  ],
  "default_attributes": {
    "openldap": {
      "clients": {
        "uri": "ldaps://ldap.ina.fr",
        "base": "o=ina",
        "sizelimit": 5000
        "certificates":{
          "GlobalSign-Root-CA": "-----BEGIN CERTIFICATE-----\nMIIDdTCCAl2gAwIBAgILBAAAAAABFUtaw5QwDQYJKoZIhvcNAQEFBQAwVzELMAkG\nA1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNVBAsTB1Jv\nb3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw05ODA5MDExMjAw\nMDBaFw0yODAxMjgxMjAwMDBaMFcxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i\nYWxTaWduIG52LXNhMRAwDgYDVQQLEwdSb290IENBMRswGQYDVQQDExJHbG9iYWxT\naWduIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDaDuaZ\njc6j40+Kfvvxi4Mla+pIH/EqsLmVEQS98GPR4mdmzxzdzxtIK+6NiY6arymAZavp\nxy0Sy6scTHAHoT0KMM0VjU/43dSMUBUc71DuxC73/OlS8pF94G3VNTCOXkNz8kHp\n1Wrjsok6Vjk4bwY8iGlbKk3Fp1S4bInMm/k8yuX9ifUSPJJ4ltbcdG6TRGHRjcdG\nsnUOhugZitVtbNV4FpWi6cgKOOvyJBNPc1STE4U6G7weNLWLBYy5d4ux2x8gkasJ\nU26Qzns3dLlwR5EiUWMWea6xrkEmCMgZK9FGqkjWZCrXgzT/LCrBbBlDSgeF59N8\n9iFo7+ryUp9/k5DPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8E\nBTADAQH/MB0GA1UdDgQWBBRge2YaRQ2XyolQL30EzTSo//z9SzANBgkqhkiG9w0B\nAQUFAAOCAQEA1nPnfE920I2/7LqivjTFKDK1fPxsnCwrvQmeU79rXqoRSLblCKOz\nyj1hTdNGCbM+w6DjY1Ub8rrvrTnhQ7k4o+YviiY776BQVvnGCv04zcQLcFGUl5gE\n38NflNUVyRRBnMRddWQVDf9VMOyGj/8N7yy5Y0b2qvzfvGn9LhJIZJrglfCm7ymP\nAbEVtQwdpf5pLGkkeB6zpxxxYu7KyJesF12KwvhHhm4qxFYxldBniYUr+WymXUad\nDKqC5JlR3XC321Y9YeRq4VzW9v493kHMB65jUr9TU/Qr6cf9tveCX4XSQRjbgbME\nHMUfpIBvFSDJ3gyICh3WZlXi/EjJKSZp4A==\n-----END CERTIFICATE-----"
        }
      }
    }
  }
}
```

#### openldap::server

```json
{
   "name":"my_node",
   "run_list": [
     "recipe[openldap::server]"
   ],
   "default_attributes": {
     "openldap": {
       "server": "true",
       "databag": "openldap",
       "schemas": [
         "core.schema",
         "cosine.schema",
         "inetorgperson.schema",
         "nis.schema",
         "collective.schema",
         "dyngroup.schema",
         "misc.schema",
         "ppolicy.schema",
         "ina.schema"
       ],
       "external_schemas": {
         "ina.schema": "http://src.infra.sas.ina/openldap/ina.schema"
       },
       "ldif_to_deploy": {
         "ina.ldif": "http://src.infra.sas.ina/openldap/ina.ldif",
         "inamediapro.ldif": "http://src.infra.sas.ina/openldap/inamediapro.ldif"
       },
       "id": {
         "ldap01.infra.i.priv.ina": 1,
         "ldap02.infra.i.priv.ina": 2
       },
       "suffixes": {
         "database": "mdb",
         "rootdn": "cn=Manager,o=ina",
         "suffix": "o=ina",
         "directory": "/usr/local/openldap/var/openldap-data/ina",
         "rootpw": "pouet"
       }       
     }
   }
}
```

Contributing
------------
Toute contribution est la bienvenue ! :-)


License and Authors
-------------------
Authors: Julien Huon (julien.huon@mairie-saint-maur.com)
