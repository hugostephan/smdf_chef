# ldap.conf properties
default['openldap']['clients']['ldap_conf'] = '/etc/openldap/ldap.conf'
default['openldap']['clients']['cacert_dir'] = '/etc/openldap/certs'
default['openldap']['clients']['uri'] = nil
default['openldap']['clients']['base'] = nil
default['openldap']['clients']['sizelimit'] = nil
default['openldap']['clients']['timelimit'] = nil
default['openldap']['clients']['deref'] = "never"

# server certificate(s)
default['openldap']['certificates'] = {}
