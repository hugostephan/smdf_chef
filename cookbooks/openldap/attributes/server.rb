default['yum']['openldap']['managed'] = true
default['openldap']['server'] = false
default['openldap']['databag'] = nil

# Configuration générale
default['openldap']['user'] = 'ldap'
default['openldap']['group'] = 'ldap'
default['openldap']['ip'] = '*'
default['openldap']['ssl_ip'] = '*'
default['openldap']['port'] = '389'
default['openldap']['ssl_port'] = '636'
case node['openldap']['server']
when true
normal['rsyslog']['default_facility_logs'] = {
  'local4.*' => "/var/log/openldap.log"
}
end
default['openldap']['loglevel'] = 'sync stats'

# limites
default['openldap']['limits'] = {
  'sizelimit'    => [
    'size.soft=5000 size.hard=5000 size.pr=5000 size.prtotal=unlimited'
   ],
  'timelimit'    => '3600',
  'idletimeout'  => '10800'
}

# TLS
default['openldap']['tls'] = {
  'enabled'		 => false,
  'directory'		 => '/usr/local/openldap/etc/openldap/tls',
  'TLSCACertificateFile' => 'openldapCA.pem',
  'TLSCertificateFile'   => 'openldap.pem',
  'TLSCertificateKeyFile'=> 'openldap.key',
  'TLSProtocolMin'       => '3.1',
  'TLSCipherSuite'       => 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSAAES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA'
}

# Securité
default['openldap']['security'] = nil

# Schémas à activer
default['openldap']['schemas'] = %w(core.schema cosine.schema inetorgperson.schema nis.schema collective.schema dyngroup.schema misc.schema ppolicy.schema)
default['openldap']['external_schemas'] = {}

# Délégation d'authentification
default['openldap']['delegate'] = false
default['openldap']['delegate_url'] = "ldaps://ldap.example.org"
default['openldap']['delegate_base'] = "dc=example,dc=org"
default['openldap']['delegate_filter'] = "(uid=%U)"
default['openldap']['delegate_auth_method'] = "bind",
default['openldap']['delegate_tls_cacert_file'] = "/etc/pki/tls/certs/ca-bundle.crt"

# ID unique des serveurs (si réplication)
default['openldap']['id'] = {}

# Suffixes
default['openldap']['suffixes'] = {
  # cn=config
  "config" => {
    'database' => 'config',
    'access' => 'to *  by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage  by * none'
  }
}

# Replications informations
default['openldap']['replications'] = {}

# LDIF à déployer
default['openldap']['ldif_to_deploy'] = {}
