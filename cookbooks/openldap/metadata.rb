name             'openldap'
maintainer       'Mairie_Saint-Maur'
maintainer_email 'julien.huon@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures openldap'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.5'

depends	'yum-openldap'
