#
# Cookbook Name:: openldap
# Provider:: certutil
#

use_inline_resources

action :add do
  # Import du certificat avec certutil 
  bash 'import_certificate' do 
    user new_resource.user
    cwd "/tmp"
    code  <<-EOH
      echo "#{new_resource.cert}" | certutil -A -d #{new_resource.cacert_dir} -t "#{new_resource.trust}" -n "#{new_resource.name}"
    EOH
   not_if do `certutil -L -d #{new_resource.cacert_dir} -n "#{new_resource.name}" -a`.include? "CERTIFICATE" end
  end

end
