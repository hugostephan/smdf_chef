# Cookbook Name:: openldap
# Provider:: config
#

use_inline_resources

action :generate do
  # Test de la configuration
  script "test_config" do
    interpreter "bash"
    user node['openldap']['user']
    cwd "/tmp"
    code "/usr/local/openldap/sbin/slaptest -f /usr/local/openldap/etc/openldap/slapd.conf -F #{new_resource.name} -u"
    action :run
  end

  # Purge du dossier cn=config
  script "purge_old_config" do
    interpreter "bash"
    user node['openldap']['user']
    cwd "/tmp"
    code "rm -rf #{new_resource.name}/cn=config*"
    action :run
  end

  # Génération de cn=config
  script "generate_cn_config_conf" do
    interpreter "bash"
    user node['openldap']['user']
    cwd "/tmp"
    code "/usr/local/openldap/sbin/slaptest -f /usr/local/openldap/etc/openldap/slapd.conf -F #{new_resource.name}"
    action :run
    returns [0,1]
  end

end
