# Cookbook Name:: openldap
# Provider:: create_account
#

use_inline_resources

action :create do

  # Le mot de passe est Hashé en SSHA
  password = SSHA.hash_password("#{new_resource.password}").gsub(/\*.*\*/, "")

  # Création de l'utilisateur
  script "create_user_#{new_resource.name}" do
    interpreter "bash"
    user "root"
    cwd "/tmp"
    code <<-EOH
    echo "dn: #{new_resource.name}
changetype: add
objectClass: simpleSecurityObject
objectClass: top
objectClass: organizationalRole
description: LDAP replicator
userPassword: #{password}

" | /usr/local/openldap/bin/ldapmodify -c -Y EXTERNAL -H ldapi:///
    EOH
    action :run
    returns [0, 68,20]
  end

  # Ajout de l'utilisateur à des groupes éventuels
  new_resource.groups.each do |group|
    script "add_user_to_group_#{group}" do
      interpreter "bash"
      user "root"
      cwd "/tmp"
      code <<-EOH
      echo "dn: #{group}
changetype: modify
add: member
member: #{new_resource.name}

" | /usr/local/openldap/bin/ldapmodify -c -Y EXTERNAL -H ldapi:///
      EOH
      action :run
      returns [0, 68,20]
    end
  end
end
