#
# Cookbook Name:: openldap
# Provider:: delegate
#

use_inline_resources

action :configure do
  
  # Installation & Configuration de Cyrus-SASL
  package "cyrus-sasl"

  template "/etc/sysconfig/saslauthd" do
    source "saslauthd.erb"
    action :create
  end

  template "/etc/saslauthd.conf" do
    source "saslauthd.conf.erb"
    action :create
    variables(
      :url	=> new_resource.url,
      :base	=> new_resource.base,
      :filter   => new_resource.filter,
	  :authmethod => new_resource.authmethod,
	  :tlscacertfile => new_resource.tlscacertfile
    )
  end

  template "/usr/lib64/sasl2/slapd.conf" do
    source "saslslapd.conf.erb"
    action :create
    mode   "644"
  end
end
