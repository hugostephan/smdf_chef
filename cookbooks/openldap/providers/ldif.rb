# Cookbook Name:: openldap
# Provider:: ldif
#

use_inline_resources

action :import do
  remote_file "#{Chef::Config[:file_cache_path]}/#{new_resource.name}" do
    source new_resource.url 
    action :create
    mode '0644'
  end

  script "import_ldif" do
    interpreter "bash"
    user "root"
    cwd "/tmp"
    code "/usr/local/openldap/bin/ldapmodify -c -Y EXTERNAL -H ldapi:/// -f #{Chef::Config[:file_cache_path]}/#{new_resource.name}"
    action :run
    returns [0,68,10,53]
  end
end
