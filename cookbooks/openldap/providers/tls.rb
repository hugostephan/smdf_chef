# Cookbook Name:: openldap
# Provider:: tls
#

use_inline_resources

action :configure do

  directory new_resource.name do
    owner new_resource.owner
    group new_resource.group
    mode '700'
    action :create
  end

  new_resource.certificate.each do |file,content|
    file "#{new_resource.name}/#{file}" do
      owner new_resource.owner
      group new_resource.group
      mode '700'
      action :create
      content content
    end
  end
end
