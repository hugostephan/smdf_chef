#
# Cookbook Name:: openldap
# Recipe:: default
#
# Copyright 2016, Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

certificates = {}

# Installation des packages clients (sauf si serveur)
['openldap', 'openldap-clients' ].each do |pkg|
  package pkg do
   not_if { node['openldap']['server'] } 
  end
end

# Génération du fichier ldap.conf (sauf si serveur)
template node['openldap']['clients']['ldap_conf'] do
  source "ldap.conf.erb"
  owner "root"
  group "root"
  mode '0644'
  variables(
    :base	=> node['openldap']['clients']['base'],
    :uri	=> node['openldap']['clients']['uri'],
    :sizelimit	=> node['openldap']['clients']['sizelimit'],
    :timelimit	=> node['openldap']['clients']['timelimit'],
    :tls_cacertdir => node['openldap']['clients']['cacert_dir'],
    :deref	=> node['openldap']['clients']['deref']
  )
  not_if { node['openldap']['server'] }
end

# Importation du/des certificat(s) du/des serveur(s) 
certificates.merge!(node['openldap']['clients']['certificates']) if node['openldap']['clients']['certificates']

certificates.each do |name,certificate|
  openldap_certutil name do
    action :add
    cacert_dir node['openldap']['clients']['cacert_dir']
    cert certificate
    trust "C"
    user "root"
  end
end
