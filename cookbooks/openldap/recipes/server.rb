#
# Cookbook Name:: openldap
# Recipe:: server
#
# Copyright 2016, Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

# Permet les merges récursifs
chef_gem "deep_merge" do
  options ( "-p http://proxy-p.dmz.saintmaur.local:3128")
  version "1.0.1"
  compile_time true
  only_if node['openldap']['server']
end

# Permet les hashs SSHA
chef_gem "ssha" do
  options ( "-p http://proxy-p.dmz.saintmaur.local:3128")
  version "0.0.3"
  compile_time true
  only_if node['openldap']['server']
end

require 'deep_merge'
require 'ssha'

include_recipe 'yum-openldap' if node['openldap']['server']

# Variables : Suffixes, Replication & certificats
suffixes = {}
suffixes.merge!(Chef::EncryptedDataBagItem.load(node['openldap']['databag'],node.chef_environment)['suffixes']) if node['openldap']['databag']
suffixes.deep_merge!(node['openldap']['suffixes']) if node['openldap']['suffixes']

tls = {}
tls.merge!(Chef::EncryptedDataBagItem.load(node['openldap']['databag'],node.chef_environment)['tls']) if node['openldap']['databag']

replications = {}
replications.merge!(Chef::EncryptedDataBagItem.load(node['openldap']['databag'],node.chef_environment)['replications']) if node['openldap']['databag']
replications.deep_merge!(node['openldap']['replications']) if node['openldap']['replications']


# Installation des packages
['openldap-ltb', 'openldap-ltb-contrib-overlays', 'openldap-ltb-mdb-utils' ].each do |pkg|
  package pkg do
   options "--enablerepo=openldap"
   only_if { node['openldap']['server'] }
  end
end

# Délégation d'authentification (si activé)
openldap_delegate "saslauthd" do
  action :configure
  url node['openldap']['delegate_url']
  base node['openldap']['delegate_base']
  filter node['openldap']['delegate_filter']
  authmethod node['openldap']['delegate_auth_method']
  tlscacertfile node['openldap']['delegate_tls_cacert_file']
  notifies :restart, "service[saslauthd]", :immediately
  only_if { node['openldap']['delegate'] }
  only_if { node['openldap']['server'] }
end

service "saslauthd" do
  action :enable
  only_if { node['openldap']['delegate'] }
  only_if { node['openldap']['server'] }
end

# Création des dossiers de chaque suffixe
node['openldap']['suffixes'].each do |name, attrs|
  if attrs['directory']
    directory attrs['directory'] do
      owner node['openldap']['user']
      group node['openldap']['group']
      mode '700'
      action :create
      only_if { node['openldap']['server'] }
    end
  end
end

# Déploiement des fichiers de schémas externes
node['openldap']['external_schemas'].each do |schema,url|
  remote_file "/usr/local/openldap/etc/openldap/schema/#{schema}" do
    source url
    action :create
    owner node['openldap']['user']
    group node['openldap']['group']
    mode '0444'
    notifies :generate, "openldap_config[/usr/local/openldap/etc/openldap/]"
    notifies :restart, "service[slapd]"
    only_if { node['openldap']['server'] }
  end
end

# Déploiement du certificat (si TLS activé)
openldap_tls node['openldap']['tls']['directory'] do
  action :configure
  certificate tls
  owner node['openldap']['user']
  group node['openldap']['group']
  notifies :restart, "service[slapd]"
  only_if { node['openldap']['tls']['enabled'] }
  only_if { node['openldap']['server'] }
end

# Création de la configuration OpenLDAP (slapd.conf + slapd)
template "/usr/local/openldap/etc/openldap/slapd.conf" do
  action :create
  source "slapd.conf.erb"
  variables(
    :schemas		=> node['openldap']['schemas'],
    :loglevel		=> node['openldap']['loglevel'],
    :size_limits	=> node['openldap']['limits']['sizelimit'],
    :time_limit		=> node['openldap']['limits']['timelimit'],
    :idle_timeout	=> node['openldap']['limits']['idletimeout'],
    :tls			=> node['openldap']['tls'],
	:security		=> node['openldap']['security'],
    :servers_id		=> node['openldap']['id'],
    :suffixes		=> suffixes,
    :replications	=> replications
  )
  owner node['openldap']['user']
  group node['openldap']['group']
  mode '0600'
  notifies :generate, "openldap_config[/usr/local/openldap/etc/openldap/]", :immediately
  only_if { node['openldap']['server'] }
end

# Déploiement de cn=config
openldap_config "/usr/local/openldap/etc/openldap/" do
  action :nothing
  user node['openldap']['user']
  notifies :restart, "service[slapd]"
  only_if { node['openldap']['server'] }
end

# Start & Enable
template "/etc/default/slapd" do
  source "slapd.erb"
  action :create
  notifies :restart, "service[slapd]"
  only_if { node['openldap']['server'] }
end

service "slapd" do
  action [:enable, :start]
  only_if { node['openldap']['server'] }
end

# Peuplement de l'annuaire
node['openldap']['ldif_to_deploy'].each do |ldif,url|
  openldap_ldif ldif do
    url url
    action :import
    only_if { node['openldap']['server'] }
  end
end

# Création des utilisateurs de réplication (si le serveur possède un ID unique et est donc master)
replications.each do |suffix,servers|
  servers.each do |server,properties|
    openldap_create_account properties['replication_account'] do
      action :create
      password properties['replication_password']
      groups properties['groups'] 
      only_if { node['openldap']['server'] }
      only_if { node['openldap']['id'][node['fqdn']] }
    end
  end
end
