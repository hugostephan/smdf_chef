actions :add
default_action :add

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true

attribute :cacert_dir,
  :kind_of => String,
  :required => true

attribute :cert,
  :kind_of => String,
  :required => true

attribute :trust,
  :kind_of => String,
  :required => true,
  :default => "C"

attribute :user,
  :kind_of => String,
  :required => true,
  :default => "root"
