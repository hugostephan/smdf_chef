actions :generate
default_action :generate

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true

attribute :user,
  :kind_of => String,
  :required => true
