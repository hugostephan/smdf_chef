actions :create
default_action :create

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true

attribute :password,
  :kind_of => String,
  :required => true

attribute :groups,
  :kind_of => Array
