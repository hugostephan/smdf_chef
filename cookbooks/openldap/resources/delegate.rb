actions :configure
default_action :configure

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true

attribute :url,
  :kind_of => String,
  :required => true

attribute :base,
  :kind_of => String,
  :required => true

attribute :filter,
  :kind_of => String,
  :required => true

attribute :authmethod,
  :kind_of => String,
  :required => true
  
attribute :tlscacertfile,
  :kind_of => String,
  :required => false