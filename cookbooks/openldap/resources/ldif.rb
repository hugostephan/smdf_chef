actions :import
default_action :import

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true

attribute :url,
  :kind_of => String,
  :required => true
