actions :configure
default_action :configure

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true

attribute :certificate,
  :kind_of => Hash,
  :required => true

attribute :owner,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true
