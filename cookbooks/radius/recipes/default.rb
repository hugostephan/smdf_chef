#
# Cookbook Name:: radius
# Recipe:: default
#
# Copyright 2019, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# generate radiusd config
template '/etc/raddb/mods-config/files/authorize' do
  source 'authorize.erb'
  owner 'radiusd'
  group 'root'
  mode '0640'
end

template '/etc/raddb/radiusd.conf' do
  source 'radiusd.conf.erb'
  owner 'radiusd'
  group 'root'
  mode '0640'
end

template '/etc/raddb/clients.conf' do
  source 'clients.conf.erb'
  owner 'radiusd'
  group 'root'
  mode '0640'
end

# restarting radiusd service
execute 'Restarting Radius Daemon' do
  command 'systemctl reload radiusd'
end