#
# Cookbook Name:: rclocal
# Recipe:: default
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute
#

template '/etc/rc.d/rc.local' do
  source 'rc.local.erb'
  owner 'root'
  group 'root'
  mode '0777'
end