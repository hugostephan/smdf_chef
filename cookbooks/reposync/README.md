# reposync Cookbook

Permet de créer son propre repository CentOS et d'en programmer sa synchronisation.


## Requirements

### Platforms

- CentOS 7

### Chef

- Chef 12.0 or later

### Cookbooks

- apache2

## Attributes




## Usage

### reposync::default

Just include `reposync` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[reposync]"
  ]
}
```

## Contributing

Allez, viens contribuer, on est bien.

## License and Authors

Authors: Julien Huon

