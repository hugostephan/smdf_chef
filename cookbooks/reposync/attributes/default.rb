# Package � installer
default['reposync']['packages'] = [ 'yum-utils', 'createrepo' ]

# Dossier qui va contenir les repositories
default['reposync']['repositories_folder'] = "/media/repositories"

# Repositories � synchroniser
default['reposync']['repositories'] = {}

# FQDN du serveur de repo
default['reposync']['hostname'] = node['fqdn']

# Logs Apache
default['reposync']['errorlog'] = "/var/log/httpd/#{node["reposync"]["hostname"]}_error.log"
default['reposync']['accesslog'] = "/var/log/httpd/#{node["reposync"]["hostname"]}_access.log combined"

