name             'reposync'
maintainer       'Mairie_Saint_Maur'
maintainer_email 'julien.huon@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures createrepo'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'apache2', '= 3.2.2'
depends 'firewalld', '= 1.1.2'