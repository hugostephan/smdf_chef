#
# Cookbook Name:: reposync
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

repo_folder = "#{node['reposync']['repositories_folder']}/#{node['platform']}/#{node['platform_version'].to_i}"

## Création des repositories ##

# Installation des packages
node['reposync']['packages'].each do |package_name|
  package package_name
end

# Création du dossier qui va contenir les repositories
directory node['reposync']['repositories_folder'] do
  notifies :run, 'execute[set_selinux_context]', :immediately
end

execute "set_selinux_context" do
  command "chcon -Rv --type=httpd_sys_content_t  #{node['reposync']['repositories_folder']}"
  action :nothing
end

directory "#{node['reposync']['repositories_folder']}/#{node['platform']}"
directory repo_folder

# Initialisation des repo & Programmation de la synchronisation
node['reposync']['repositories'].each do |repo_name,repo_properties|
  directory "#{repo_folder}/#{repo_name}" do
    owner "root"
    group "root"
    action :create
	recursive true
  end
  
  file "#{repo_folder}/#{repo_name}/mirrorlist.html" do
    content "http://#{node['reposync']['hostname']}/#{node['platform']}/#{node['platform_version'].to_i}/#{repo_name}"
  end
	
  execute "createrepo_#{repo_name}" do
    command "createrepo #{repo_folder}/#{repo_name}"
    not_if { ::Dir.exist?( "#{repo_folder}/#{repo_name}/repodata" ) }
  end
	
  cron repo_name do
    action :create
    minute repo_properties['minute']
    hour repo_properties['hour']
    user "root"
    command "reposync --download-metadata --downloadcomps -d -r #{repo_name} -p #{repo_folder} >/dev/null && createrepo --update #{repo_folder}/#{repo_name} >/dev/null ||  echo 'Subject: ECHEC du cron reposync sur #{repo_folder}/#{repo_name}' | /usr/sbin/sendmail #{repo_properties['mailto']}"
  end
end

## Installation/Configuration d'Apache ##

include_recipe "apache2"

web_app "repo" do
  template 'repo.conf.erb'
  server_name node['reposync']['hostname']
end

firewalld_service 'http' do
  action :add
  zone   'public'
end