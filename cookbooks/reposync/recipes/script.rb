#
# Cookbook Name:: reposync
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

repo_folder = "#{node['reposync']['repositories_folder']}/#{node['platform']}/#{node['platform_version'].to_i}"

## Création des repositories ##

# Installation des packages
node['reposync']['packages'].each do |package_name|
  package package_name
end

# Création du dossier qui va contenir les repositories
directory node['reposync']['repositories_folder'] do
  notifies :run, 'execute[set_selinux_context]', :immediately
end

execute "set_selinux_context" do
  command "chcon -Rv --type=httpd_sys_content_t  #{node['reposync']['repositories_folder']}"
  action :nothing
end

directory "#{node['reposync']['repositories_folder']}/#{node['platform']}"
directory repo_folder


# Chargement du template script
template '/tmp/reposcript' do
  source 'reposcript.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

execute "grant executing script" do
  command 'chmod a+x /tmp/reposcript'
end

# cron do
	# action :create
	# minute 0
	# hour 23
	# user 'root' 
	# command '~/reposcript'
# end


## Installation/Configuration d'Apache ##

include_recipe "apache2"

web_app "repo" do
  template 'repo.conf.erb'
  server_name node['reposync']['hostname']
end

firewalld_service 'http' do
  action :add
  zone   'public'
end