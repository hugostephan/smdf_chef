#
# Cookbook Name:: selenium
# Attributes:: default
# Auuthor : Hugo STEPHAN
# Copyright : Mairie SMDF, 2018
#

## Node Default Config
default['selenium']['node']['browserTimeout'] = 60
default['selenium']['node']['cleanUpCycle'] = 2500
default['selenium']['node']['debug'] = false
default['selenium']['node']['downPollingLimit'] = 2
default['selenium']['node']['proxy'] = 'org.openqa.grid.selenium.proxy.DefaultRemoteProxy'
default['selenium']['node']['hub'] = 'http://localhost:4444'
default['selenium']['node']['jettyThreads'] = 300
default['selenium']['node']['log'] = '/var/log/selenium.log'
default['selenium']['node']['maxSession'] = 10
default['selenium']['node']['nodePolling'] = 2500
default['selenium']['node']['nodeStatusCheckTimeout'] = 2500
default['selenium']['node']['port'] = 5555
default['selenium']['node']['register'] = true
default['selenium']['node']['registerCycle'] = 5000
default['selenium']['node']['role'] = 'node'
default['selenium']['node']['servlet'] = 'org.openqa.grid.web.servlet.LifecycleServlet'
default['selenium']['node']['timeout'] = 45
default['selenium']['node']['unregisterIfStillDownAfter'] = 5000

default['selenium']['node']['capabilities']['browserName'] = 'chrome'
default['selenium']['node']['capabilities']['maxInstances'] = 10
default['selenium']['node']['capabilities']['seleniumProtocol'] = 'WebDriver'

# Hub Default Config
default['selenium']['hub']['browserTimeout'] = 60
default['selenium']['hub']['capabilityMatcher'] = 'org.openqa.grid.internal.utils.DefaultCapabilityMatcher'
default['selenium']['hub']['cleanUpCycle'] = 2500
default['selenium']['hub']['debug'] = false
default['selenium']['hub']['jettyThreads'] = 300
default['selenium']['hub']['log'] = '/var/log/selenium.log'
default['selenium']['hub']['maxSession'] = 10
default['selenium']['hub']['newSessionWaitTimeout'] = 75_000
default['selenium']['hub']['port'] = 4444
default['selenium']['hub']['role'] = 'hub'
default['selenium']['hub']['servlet'] = 'org.openqa.grid.web.servlet.LifecycleServlet'
default['selenium']['hub']['throwOnCapabilityNotPresent'] = true
default['selenium']['hub']['timeout'] = 60
