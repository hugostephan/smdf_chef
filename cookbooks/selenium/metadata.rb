name             'selenium'
maintainer       'Mairie SMDF'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures selenium'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends 'yum-google-chrome'
