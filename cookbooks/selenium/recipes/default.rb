# Cookbook Name:: selenium
# Recipe:: default
# Install and configure selenium host
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute

# Required configuration
package 'Xvfb'
package 'java-1.8.0-openjdk.x86_64'

# generic system config to run selenium
execute 'Adding selenium user' do
  command 'useradd selenium > /dev/null 2>1 || exit 0'
end
execute 'Enabling rc.local' do
  command 'chmod u+x /etc/rc.local'
end
execute 'Opening firewall for selenium communications' do
  command 'firewall-cmd --add-port=4444/tcp --permanent ; firewall-cmd --add-port=5555/tcp --permanent'
end

# Init Log files
execute 'Creating log dir' do
  command 'mkdir -p /var/log/selenium/'
end
execute 'Creating error log file' do
  command '[ -f /var/log/selenium/selenium.log ] &&  echo "Error log file already exists" || touch /var/log/selenium/selenium.log ; chmod 777 /var/log/selenium/selenium.log'
end
execute 'Creating message log file' do
  command '[ -f /var/log/selenium/selenium.out ] &&  echo "Message log file already exists" || touch /var/log/selenium/selenium.out ; chmod 777 /var/log/selenium/selenium.out'
end
execute 'Checking selenium log rights' do
  command 'chmod 777 -R /var/log/selenium/ ; chown -R selenium /var/log/selenium/'
end

# Installing Selenium Server Jar
execute 'Installing selenium if not installed' do
  command '[ -f /opt/selenium/selenium-server-standalone-3.11.0.jar ] && echo "Selenium Server already installed : nothing to do." || (mkdir -p /opt/selenium ; wget http://selenium-release.storage.googleapis.com/3.11/selenium-server-standalone-3.11.0.jar ; mv ./selenium-server-standalone-3.11.0.jar /opt/selenium/ -f)'
end

# cleaning log file every 6 hours
cron 'cleaning log file' do
  minute '0'
  hour '*/6'
  day '*'
  month '*'
  command 'echo "" > /var/log/selenium/selenium.log'
  user 'root'
end
