# Cookbook Name:: selenium
# Recipe:: hub
#
# Push JSON Config for selenium server hub host
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute

include_recipe '::default'

# Hub config JSON file
template '/opt/selenium/hub.config.json' do
  source 'hub.config.json.erb'
  owner 'root'
  group 'root'
  mode '0666'
end

# Configuring various cron task to clean & stabilize hub (by rebooting nodes gracefully)
cron 'cleaning screenshots dir' do
  minute '*/5'
  hour '*'
  day '*'
  month '*'
  command 'cd /opt/AppMonitor; rm -rf screenshots/*.png'
  user 'root'
end

cron 'cleaning waiting nsca queries' do
  minute '0'
  hour '23'
  day '*'
  month '*'
  command 'killall nsca > /dev/null 2>&1'
  user 'root'
end

cron 'rebooting node test01' do
  minute '15'
  hour '*/6'
  day '*'
  month '*'
  command 'ssh root@test01-p.saintmaur.local "curl http://localhost:5555/extra/LifecycleServlet?action=shutdown ; sleep 50; reboot" > /dev/null 2>&1'
  user 'root'
end

cron 'rebooting node test02' do
  minute '30'
  hour '*/6'
  day '*'
  month '*'
  command 'ssh root@test02-p.saintmaur.local "curl http://localhost:5555/extra/LifecycleServlet?action=shutdown ; sleep 50; reboot" > /dev/null 2>&1'
  user 'root'
end

cron 'rebooting node test03' do
  minute '45'
  hour '*/6'
  day '*'
  month '*'
  command 'ssh root@test03-p.saintmaur.local "curl http://localhost:5555/extra/LifecycleServlet?action=shutdown ; sleep 50; reboot" > /dev/null 2>&1'
  user 'root'
end
