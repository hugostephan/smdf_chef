# Cookbook Name:: selenium
# Recipe:: node
#
# Push JSON Config for selenium server node host
#
# Copyright 2018, Mairie SMDF
#
# All rights reserved - Do Not Redistribute

include_recipe '::default'
include_recipe 'yum-google-chrome'

# Installing Chrome & Chrome Driver
package 'google-chrome-stable'
execute 'Installing chrome-driver if not installed' do
  command '[ -f /usr/bin/chromedriveru ] && echo "Chrome driver already installed : nothing to do." || (wget https://chromedriver.storage.googleapis.com/2.39/chromedriver_linux64.zip ; unzip chromedriver_linux64.zip ; rm -rf chromedriver_linux64.zip ; mv ./chromedriver /usr/bin/chromedriver -f)'
end

# Node config JSON file
template '/opt/selenium/node.config.json' do
  source 'node.config.json.erb'
  owner 'root'
  group 'root'
  mode '0666'
end

# Configuring various cron task to clean & stabilize node
cron 'cleaning driver' do
  minute '*/2'
  hour '*'
  day '*'
  month '*'
  command '/usr/bin/killall -u selenium -o 3m chrome chromedriver AudioThread cat > /dev/null 2>&1'
  user 'root'
end

cron 'cleaning session .org' do
  minute '*/2'
  hour '*'
  day '*'
  month '*'
  command '/usr/bin/find /tmp/.org.* -type d -cmin +2 -prune -exec rm -rf {} \; > /dev/null 2>&1'
  user 'root'
end

cron 'cleaning session .com' do
  minute '*/2'
  hour '*'
  day '*'
  month '*'
  command '/usr/bin/find /tmp/.com.* -type d -cmin +2 -prune -exec rm -rf {} \; > /dev/null 2>&1'
  user 'root'
end

cron 'cleaning session .fr' do
  minute '*/2'
  hour '*'
  day '*'
  month '*'
  command '/usr/bin/find /tmp/.fr.* -type d -cmin +2 -prune -exec rm -rf {} \; > /dev/null 2>&1'
  user 'root'
end

cron 'cleaning tmp dir' do
  minute '*/2'
  hour '*'
  day '*'
  month '*'
  command 'rmdir /tmp/* > /dev/null 2>&1'
  user 'root'
end
