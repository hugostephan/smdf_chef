# shibboleth-idp CHANGELOG

This file is used to list changes made in each version of the shibboleth-idp cookbook.

## 0.1.0
- [your_name] - Initial release of shibboleth-idp

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
