# Oracle JDK + JCE
default['java']['jdk_version'] = '8'
default['java']['install_flavor'] = 'oracle'
default['java']['set_etc_environment'] = true
default['java']['oracle']['accept_oracle_download_terms'] = true
default['java']['oracle']['jce']['enabled'] = true

# Jetty 9
default['jetty']['install_java'] = nil
default['jetty']['create_base_instance'] = nil
default['jetty']['instances'] = {
  "shibboleth" => {
    "http_port" => 8080,
    "jetty_user" => "shibboleth",
    "jetty_group" => "shibboleth",
    "java_options" => "-server -Xms128m -Xmx384m -XX:+DisableExplicitGC"
  }
}

# Apache
default['apache']['serversignature'] = 'Off'
default['shibboleth-idp']['vhosts'] = {
  "shibboleth" => {
    "proxypass" => [
      "/idp http://127.0.0.1:8080/idp"
     ],
    "proxypassreverse" => [
      "/idp http://127.0.0.1:8080/idp"
     ],
    "proxypreservehost" => "on",
    "requestheader" => [
      "set X-Forwarded-Proto \"https\"",
      "set X-Forwarded-Port \"443\"",
      "set REMOTE-USER %{REMOTE_USER}s"
    ],
	"remoteipheader" => [],
	"remoteipinternalproxy" => []
  }
}

# Shibboleth-idp
default['shibboleth-idp']['zip_repository'] = "https://shibboleth.net/downloads/identity-provider/"
default['shibboleth-idp']['version'] = "3.2.1"
default['shibboleth-idp']['checksum'] = '9d509e4c54cba5de8b63fba9a59c90138f289f8126ab7849f8a42392432a1317'

default['shibboleth-idp']['user'] = "shibboleth"
default['shibboleth-idp']['group'] = "shibboleth"
default['shibboleth-idp']['home_dir'] = "/opt/shibboleth-idp"

default['shibboleth-idp']['host_name'] = node['fqdn']
default['shibboleth-idp']['entity_id'] = "https://#{node['fqdn']}/idp"
default['shibboleth-idp']['sealer_password'] = 'changeit'
default['shibboleth-idp']['keystore_password'] = 'changeit'
default['shibboleth-idp']['scope'] = node['domain']
default['shibboleth-idp']['enable_SLO'] = FALSE

# Authorized IP for status, resolvertest & reload
default['shibboleth-idp']['access_policy']['status'] = [
  "127.0.0.1/32",
  "::1/128"
]

default['shibboleth-idp']['access_policy']['resolver'] = [
  "127.0.0.1/32",
  "::1/128"
]

default['shibboleth-idp']['access_policy']['reload'] = [
  "127.0.0.1/32",
  "::1/128"
]

# Databags
default['shibboleth-idp']['passwords_databag'] = nil
default['shibboleth-idp']['certs_databag'] = nil
default['shibboleth-idp']['keys_databag'] = nil
default['shibboleth-idp']['authn_databag'] = nil
default['shibboleth-idp']['resolver_databag'] = nil
default['shibboleth-idp']['filter_databag'] = nil
default['shibboleth-idp']['relying_parties_databag'] = nil
default['shibboleth-idp']['cas_applications_databag'] = nil
default['shibboleth-idp']['nameid_databag'] = nil

# Storage
default['shibboleth-idp']['session_storage'] = 'shibboleth.ClientSessionStorageService'
default['shibboleth-idp']['consent_storage'] = 'shibboleth.ClientPersistentStorageService'
default['shibboleth-idp']['replaycache_storage'] = 'shibboleth.StorageService'
default['shibboleth-idp']['artifact_storage'] = 'shibboleth.StorageService'
default['shibboleth-idp']['cas_storage'] = 'shibboleth.StorageService'

# If sessions, CAS tickets and artifacts are on a database...
default['shibboleth-idp']['mysql_storage_databag'] = nil
default['shibboleth-idp']['hikaricp_url'] = 'http://repo.gradle.org/gradle/repo/com/zaxxer/HikariCP/2.4.4/'
default['shibboleth-idp']['hikaricp_checksum'] = '210d7f3021a6059195b879d18eaba74f43a5fbb3850a7e37b055a30f09ba6726'
