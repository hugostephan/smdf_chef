# Option : Base de données dédié au stockage des sessions (Permet la HA de ces dernières)
default['shibboleth-idp']['mysql_database'] = 'shibboleth'
# Databag dédié au stockage des utilisateurs/passwords de la base de données
default['shibboleth-idp']['mysql_users_databag'] = nil

