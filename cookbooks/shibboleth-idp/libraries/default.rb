def get_password(user)
  # Si un databag a été passé en attribut
  if node['shibboleth-idp']['passwords_databag']
    return data_bag_item(node['shibboleth-idp']['passwords_databag'],node.chef_environment)[user]
  else
    return node['shibboleth-idp']["#{user}"]
  end
end

def get_cert(type)
  if type == "idp_signing"
    return File.open("#{node['shibboleth-idp']['home_dir']}/credentials/idp-signing.crt").read.gsub(/-----.*CERTIFICATE-----/, '').strip
  elsif type == "idp_backchannel"
    return File.open("#{node['shibboleth-idp']['home_dir']}/credentials/idp-backchannel.crt").read.gsub(/-----.*CERTIFICATE-----/, '').strip
  elsif type == "idp_encryption"
    return File.open("#{node['shibboleth-idp']['home_dir']}/credentials/idp-encryption.crt").read.gsub(/-----.*CERTIFICATE-----/, '').strip
  end
end
