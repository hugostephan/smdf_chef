name             'shibboleth-idp'
maintainer       'Mairie_Saint-Maur'
maintainer_email 'julien.huon@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures shibboleth-idp'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'java', '>= 1.41'
depends 'jetty', '>= 0.1.2'
depends 'apache2', '>= 3.2.2'
depends 'selinux_policy', '>= 0.9.5'
depends 'mysql', '>= 8.0.1'
depends 'mysql_prov', '>= 0.1.0'
depends 'yum-mysql-community', '>= 0.3.0'