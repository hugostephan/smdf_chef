#
# Cookbook Name:: shibboleth-idp
# Provider:: authn
#

use_inline_resources

action :create do

  if new_resource.name == "LDAP"
    # On créer le fichier de properties
    template "#{node['shibboleth-idp']['home_dir']}/conf/ldap-authn.properties" do
      source 'ldap-authn.properties'
      mode '0600'
      owner new_resource.owner
      group new_resource.group
      variables(
        # Choix : anonSearchAuthenticator, bindSearchAuthenticator, directAuthenticator, adAuthenticator
        :authenticator     => new_resource.properties['authenticator'],
        # Attributs obligatoires :
        :ldapURL           => new_resource.properties['ldapURL'],
        # Attributs acultatifs :
        :useStartTLS       => new_resource.properties['useStartTLS'],
        :useSSL            => new_resource.properties['useSSL'],
        :sslConfig         => new_resource.properties['sslConfig'],
        :trustCertificates => new_resource.properties['trustCertificates'],
        :trustStore        => new_resource.properties['trustStore'],
        # Si anonSearchAuthenticator & bindSearchAuthenticator :
        #   Obligatoires pour les deux :
        :baseDN            => new_resource.properties['baseDN'],  
        #   Obligatoires pour bindSearchAuthenticator uniquement :
        :bindDN            => new_resource.properties['bindDN'],
        :bindDNCredential  => new_resource.properties['bindDNCredential'],
        #   Facultatifs pour les deux :
        :subtreeSearch     => new_resource.properties['subtreeSearch'],
        :userFilter        => new_resource.properties['userFilter'],
        :connectTimeout    => new_resource.properties['connectTimeout'],
        :returnAttributes  => new_resource.properties['returnAttributes'],
        :pool              => new_resource.properties['pool'],
        # Si directAuthenticator, adAuthenticator
        #   Obligatoire :
        :dnFormat          => new_resource.properties['dnFormat']
      )
    end
  elsif new_resource.name == "SPNEGO"
    # On configure le fichier authn/spnego-authn-config.xml
    template "#{node['shibboleth-idp']['home_dir']}/conf/authn/spnego-authn-config.xml" do 
      source 'spnego-authn-config.xml.erb'
      mode '0600'
      owner new_resource.owner
      group new_resource.group
      variables(
        :properties	   => new_resource.properties
      )
    end
	
	# Transformation du login si nécessaire
	template "#{node['shibboleth-idp']['home_dir']}/conf/c14n/simple-subject-c14n-config.xml" do 
      source 'simple-subject-c14n-config.xml.erb'
      mode '0600'
      owner new_resource.owner
      group new_resource.group
      variables(
        :properties	   => new_resource.properties
      )
    end
  end
end

