#
# Cookbook Name:: shibboleth-idp
# Provider:: filter
#

action :create do
  # cas-protocol deployment
  template "#{node['shibboleth-idp']['home_dir']}/conf/cas-protocol.xml" do
    source "cas-protocol.xml.erb"
    owner new_resource.owner
    group new_resource.group
    mode "0600"
    variables(
      :cas_applications  => new_resource.cas_applications,
    )
    notifies :run, 'execute[reload_shibboleth_cas]', :delayed
  end

  # Recharge la conf cas-protocol.xml
  execute "reload_shibboleth_cas" do
    command "./reload-service.sh -id shibboleth.ReloadableCASServiceRegistry"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end

end

