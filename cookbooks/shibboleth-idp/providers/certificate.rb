#
# Cookbook Name:: shibboleth-idp
# Provider:: certificate
#

action :create do
  file "#{new_resource.name}" do
    content new_resource.content
    owner new_resource.owner
    group new_resource.group
    mode '0644'
  end

end
