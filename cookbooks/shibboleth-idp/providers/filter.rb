#
# Cookbook Name:: shibboleth-idp
# Provider:: filter
#

action :create do
  # Génération du fichier attribute-filter.xml
  template "#{node['shibboleth-idp']['home_dir']}/conf/#{new_resource.name}.xml" do
    source 'attribute-filter.xml.erb'
    mode '0600'
    owner new_resource.owner
    group new_resource.group
    variables(
      :filters => new_resource.filters
    )
    notifies :run, 'execute[reload_shibboleth_filter]', :delayed
  end

  # Recharge la conf attribute filter
  execute "reload_shibboleth_filter" do
    command "./reload-service.sh -id shibboleth.AttributeFilterService"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end

end

