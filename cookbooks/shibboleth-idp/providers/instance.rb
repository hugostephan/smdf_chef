#
# Cookbook Name:: jetty
# Provider:: instance
#

use_inline_resources

action :install do
  # User and Group creation
  group new_resource.group do
    system true
    action :create
  end

  user new_resource.user do
    system true
    shell "/bin/bash"
    group new_resource.group
    action :create
  end
  
  # Download ZIP installer
  zip_name = "shibboleth-identity-provider-#{new_resource.version}.zip"
  src_dir = "#{Chef::Config[:file_cache_path]}/shibboleth-identity-provider-#{new_resource.version}"
  
  remote_file "#{Chef::Config[:file_cache_path]}/#{zip_name}" do
    source "#{new_resource.repository}#{new_resource.version}/#{zip_name}"
    action :create
    mode "0755"
    owner "root"
    group "root"
    checksum new_resource.checksum
    notifies :run, 'execute[extract_shibboleth_idp_installer]', :immediately
  end
  
  # Extract ZIP installer
  execute "extract_shibboleth_idp_installer" do
    command "unzip #{zip_name}"
    cwd Chef::Config[:file_cache_path]
    user "root"
    group "root"
    action :nothing
    notifies :create, "template[#{src_dir}/bin/install.properties]", :immediately
  end
  
  # Installer properties file creation
  template "#{src_dir}/bin/install.properties" do
    source "install.properties.erb"
    variables( 
      :src_dir => src_dir,
	  :target_dir => new_resource.name,
	  :host_name => new_resource.host_name,
	  :entity_id => new_resource.entity_id,
      :keystore_password => new_resource.keystore_password,
      :sealer_password   => new_resource.sealer_password,
	  :scope => new_resource.scope
    )
    mode "0600"
    owner "root"
    group "root"
    notifies :run, 'execute[run_shibboleth_idp_installer]', :immediately
    action :nothing
  end
  
  # Installer execution
  execute "run_shibboleth_idp_installer" do
    command "bin/install.sh \
		-Didp.src.dir=#{src_dir} \
		-Didp.target.dir=#{new_resource.name} \
		-Didp.merge.properties=bin/install.properties \
		-Didp.sealer.password=#{new_resource.sealer_password} \
        -Didp.keystore.password=#{new_resource.keystore_password} \
		-Didp.host.name=#{new_resource.host_name} \
		-Didp.scope=#{new_resource.scope} && chown -R #{new_resource.user}:#{new_resource.group} #{new_resource.name}"
    cwd "#{src_dir}"
    action :nothing
  end
  
  # Change shibboleth files owner
  ::Dir.glob("#{new_resource.name}/**/*").each do |path|
    directory path do 
      owner new_resource.user
      group new_resource.group
    end
  end
  
  # Deploy in Jetty
  template "#{node['jetty']['instances_dir']}/base/webapps/idp.xml" do
    source "idp.xml.erb"
	variables( 
	  :home_dir => new_resource.name
	)
    owner node['jetty']['base_jetty_user']
    group node['jetty']['base_jetty_group']
    mode '0644'
	only_if { node['jetty']['create_base_instance'] }
  end  

  node['jetty']['instances'].each do |name, attrs|
    template "#{node['jetty']['instances_dir']}/#{name}/webapps/idp.xml" do
      source "idp.xml.erb"
	  variables( 
	    :home_dir => new_resource.name
	  )
      owner node['jetty']['instances']["#{name}"]['jetty_user']
      group node['jetty']['instances']["#{name}"]['jetty_group']
      mode '0644'
    end
  end
   
  # Deploy Apache configuration
  new_resource.vhosts.each do |name, attrs|
    shibboleth_idp_vhost "#{name}" do
      proxypass attrs['proxypass']
      proxypassreverse attrs['proxypassreverse'] 
      proxypreservehost attrs['proxypreservehost'] 
      requestheader attrs['requestheader']
      remoteipheader attrs['remoteipheader']	
	  remoteipinternalproxy attrs['remoteipinternalproxy']
      certificatefile "/etc/pki/tls/certs/localhost.crt"
      certificatekeyfile "/etc/pki/tls/private/localhost.key"
      ldaptrustedglobalcert attrs['ldaptrustedglobalcert']
      locations attrs['locations']
      action :configure
	  not_if { ::File.exist?("#{node['apache']['dir']}/sites-available/#{name}.conf") }
    end
  end
end

action :configure do
  if ::File.exist?("/tmp/restart_jetty") 
    ::File.delete("/tmp/restart_jetty")
  end

  # Certificate deployment
  if new_resource.certs_databag
    data_bag_item(new_resource.certs_databag,node.chef_environment)['certificates'].each do |cert_name,cert_content|
      shibboleth_idp_certificate "#{new_resource.name}/credentials/#{cert_name}.crt" do
	    content cert_content
		owner new_resource.user
		group new_resource.group
        action :create
      end
    end
  end
  
  # Private keys deployment
  if new_resource.keys_databag
    data_bag_item(new_resource.keys_databag,node.chef_environment)['keys'].each do |key_name,key_properties|
      shibboleth_idp_privatekey "#{new_resource.name}/credentials/#{key_name}" do
	    content key_properties['key']
		format key_properties['format']
		owner new_resource.user
		group new_resource.group
        action :create
      end
    end
  end
  
  # Deploy Apache configuration
  new_resource.vhosts.each do |name, attrs|
    shibboleth_idp_vhost "#{name}" do
      proxypass attrs['proxypass']
      proxypassreverse attrs['proxypassreverse'] 
      proxypreservehost attrs['proxypreservehost'] 
      requestheader attrs['requestheader']
      remoteipheader attrs['remoteipheader']	
	  remoteipinternalproxy attrs['remoteipinternalproxy']
      certificatefile attrs['certificatefile']
      certificatekeyfile attrs['certificatekeyfile']
      certificatecafile attrs['certificatecafile']
      ldaptrustedglobalcert attrs['ldaptrustedglobalcert']
      locations attrs['locations']
      action :configure
    end
  end
  
  # IDP Metadata creation
  shibboleth_idp_metadata "idp-metadata" do
    nameid_databag new_resource.nameid_databag
    owner new_resource.user
    group new_resource.group
    action :create  
  end
  
  # Authentications deployment
  auth = []
  if new_resource.authn_databag
    data_bag_item(new_resource.authn_databag,node.chef_environment)['authn'].each do |authn,authn_properties|
      auth.push(authn)
	  
	  shibboleth_idp_authn authn do
		properties authn_properties
		owner new_resource.user
		group new_resource.group
		action :create
		notifies :create, 'file[/tmp/restart_jetty]', :immediately
      end
	end
  end
  
  # Attributes Resolvers deployment
  if new_resource.resolvers_databag
    # Resolver file
    data_bag_item(new_resource.resolvers_databag,node.chef_environment)['resolvers'].each do |resolver,resolver_properties|
      shibboleth_idp_resolver resolver do
		properties resolver_properties
		owner new_resource.user
		group new_resource.group
		action :create
      end
	end
	
	# Activation in services.xml
	shibboleth_idp_services "services" do
      resolvers data_bag_item(new_resource.resolvers_databag,node.chef_environment)['resolvers']
      owner new_resource.user
      group new_resource.group
      action :create
	  notifies :create, 'file[/tmp/restart_jetty]', :immediately
    end
  end
  
  # Attributes filters deployment
  if new_resource.filters_databag
    shibboleth_idp_filter "attribute-filter" do
      filters data_bag_item(new_resource.filters_databag,node.chef_environment)['filters']
	  owner new_resource.user
	  group new_resource.group
      action :create
    end
  end
  
  # Relying Parties deployment
  if new_resource.relying_parties_databag
    shibboleth_idp_relyingparties "relying-party" do
      relyingparties data_bag_item(new_resource.relying_parties_databag,node.chef_environment)['relying_parties']
	  owner new_resource.user
	  group new_resource.group
      action :create
    end
  end
  
  # MySQL/MariaDB client configuration deployment
  if node['shibboleth-idp']['session_storage'] == "shibboleth.JPAStorageService"
    package 'mysql-connector-java'
    shibboleth_idp_jpastorage 'mysql' do
      owner new_resource.user
      group new_resource.group
	  mysql_storage_databag new_resource.mysql_storage_databag
      action :create
	  notifies :create, 'file[/tmp/restart_jetty]', :immediately
    end
  end
  
  # CAS deployment
  if new_resource.cas_applications_databag
    shibboleth_idp_cas "cas" do
	  owner new_resource.user
      group new_resource.group
	  cas_applications data_bag_item(new_resource.cas_applications_databag,node.chef_environment)['cas_applications']
	end
  end
  
  # NameID deployment
  if new_resource.nameid_databag
    shibboleth_idp_nameid "nameid" do
	  owner new_resource.user
      group new_resource.group
	  nameid data_bag_item(new_resource.nameid_databag,node.chef_environment)['nameid']
      action :create
    end
  end
  
  # idp.properties file
  template "#{node['shibboleth-idp']['home_dir']}/conf/idp.properties" do
    source "idp.properties.erb"
    owner new_resource.user
    group new_resource.group
    mode "0600"
    variables(
      :sealer_password   => get_password('sealer_password'),
      :auth	       => auth
    )
    notifies :create, 'file[/tmp/restart_jetty]', :immediately
  end

  # access-control deployment
  template "#{node['shibboleth-idp']['home_dir']}/conf/access-control.xml" do
    source "access-control.xml.erb"
    owner new_resource.user
    group new_resource.group
    mode "0600"
    notifies :run, 'execute[reload_shibboleth_access]', :delayed
  end

  # Recharge la conf access-control.xml
  execute "reload_shibboleth_access" do
    command "./reload-service.sh -id shibboleth.ReloadableAccessControlService"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end
    
  # Restart Jetty
  file "/tmp/restart_jetty" do
    action :nothing
  end
  
  node['jetty']['instances'].each do |name, attrs|
    service "jetty-#{name}" do
      action :restart
      only_if { ::File.exist?("/tmp/restart_jetty") }
    end
  end
  
end