#
# Cookbook Name:: shibboleth-idp
# Provider:: jpastorage
#

use_inline_resources

action :create do
  # Copie des drivers dans edit-webapp/WEB-INF/lib
  remote_file "#{node['shibboleth-idp']['home_dir']}/edit-webapp/WEB-INF/lib/hikaricp.jar" do
    source node['shibboleth-idp']['hikaricp_url']
	checksum node['shibboleth-idp']['hikaricp_cheksum']
    owner new_resource.owner
    group new_resource.group
    mode '0644'
    notifies :run, 'execute[rebuild_shibboleth_war]', :immediately
  end

  file "#{node['shibboleth-idp']['home_dir']}/edit-webapp/WEB-INF/lib/mysql-connector-java.jar" do
    content IO.read("/usr/share/java/mysql-connector-java.jar")
    action :create
    owner new_resource.owner
    group new_resource.group
    mode '0644'
    notifies :run, 'execute[rebuild_shibboleth_war]', :immediately
  end

  # Build le war Shibboleth (idp.war)
  execute "rebuild_shibboleth_war" do
    command "./build.sh"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user new_resource.owner
    group node['shibboleth-idp']['group']
    action :nothing
    retries 3
    retry_delay 30
  end

  jdbc_url = data_bag_item(new_resource.mysql_storage_databag,node.chef_environment)["#{node['fqdn']}"]['jdbc_url']
  jdbc_username = data_bag_item(new_resource.mysql_storage_databag,node.chef_environment)["#{node['fqdn']}"]['jdbc_username']
  jdbc_password = data_bag_item(new_resource.mysql_storage_databag,node.chef_environment)["#{node['fqdn']}"]['jdbc_password']

  # déploiement du fichier global.xml
  template "#{node['shibboleth-idp']['home_dir']}/conf/global.xml" do
    source "global.xml.erb"
    owner new_resource.owner
    group new_resource.group
    mode "0600"
    variables(
      :jdbc_url   => jdbc_url,
      :username   => jdbc_username,
      :password   => jdbc_password
    )
  end

end
