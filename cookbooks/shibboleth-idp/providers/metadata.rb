#
# Cookbook Name:: shibboleth-idp
# Provider:: metadata
#

action :create do

  nameIDs = []

  # On récupère les différents nameID proposés
  if new_resource.nameid_databag
    data_bag_item(new_resource.nameid_databag,node.chef_environment)['nameid'].each do |properties|
      if properties['saml1']
        if not nameIDs.include?(properties['saml1']['format'])
          nameIDs.push(properties['saml1']['format'])
        end
      end

      if properties['saml2']
        if not nameIDs.include?(properties['saml2']['format'])
          nameIDs.push(properties['saml2']['format'])
        end
      end
    end
  end

  # On génère le fichier idp-metadata.xml
  template "#{node['shibboleth-idp']['home_dir']}/metadata/#{new_resource.name}.xml" do
    source "idp-metadata.xml.erb"
    owner new_resource.owner
    group new_resource.group
    mode '0644'
    variables(
      :sign_cert => get_cert('idp_signing'),
      :sign_backchannel_cert => get_cert('idp_backchannel'),
      :crypt_cert => get_cert('idp_encryption'),
      :nameids => nameIDs
    )
  end  

end
