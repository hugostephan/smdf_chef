#
# Cookbook Name:: shibboleth-idp
# Provider:: nameid
#

action :create do
 
  template "#{node['shibboleth-idp']['home_dir']}/conf/saml-nameid.xml" do
    source "saml-nameid.xml.erb"
    owner new_resource.owner
    group new_resource.group
    mode '0644'
    variables(
      :nameIDs  => new_resource.nameid
    )
    notifies :run, 'execute[reload_shibboleth_nameid]', :delayed
  end

  # Recharge la conf nameid
  execute "reload_shibboleth_nameid" do
    command "./reload-service.sh -id shibboleth.NameIdentifierGenerationService"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end

end
