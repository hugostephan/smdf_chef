#
# Cookbook Name:: shibboleth-idp
# Provider:: privatekey
#

action :create do
  if new_resource.format == "key"
    file "#{new_resource.name}.key" do
      content new_resource.content
      owner new_resource.owner
      group new_resource.group
      mode '0600'
    end
  elsif new_resource.format == "apachekey"
    file "#{new_resource.name}.key" do
      content new_resource.content
      owner node['apache']['user']
      group node['apache']['group']
      mode '0600'
    end
  elsif new_resource.format =="p12"
    bash 'create_backchannel_p12' do
      cwd "/tmp"
      code <<-EOH
        echo "#{new_resource.content}" | base64 -d > #{new_resource.name}.p12
        chown #{new_resource.owner}:#{new_resource.group} #{new_resource.name}.p12
        chmod 600 #{new_resource.name}.p12
      EOH
    end
  elsif new_resource.format == "sealer"
    # Sealer (format jks) with the version file(format kver)
    bash 'create_sealer' do
      cwd "/tmp"
      code <<-EOH
        echo "#{new_resource.content['jks']}" | base64 -d > #{new_resource.name}.jks
        echo "#{new_resource.content['kver']}" | base64 -d > #{new_resource.name}.kver
        chown #{new_resource.owner}:#{new_resource.group} #{new_resource.name}.jks #{new_resource.name}.kver
        chmod 600 #{new_resource.name}.jks #{new_resource.name}.kver
      EOH
    end
  elsif new_resource.format == "keytab"
    # Keytab
    bash 'create_keytab' do
      cwd "tmp"
      code <<-EOH
        echo "#{new_resource.content}" | base64 -d > #{new_resource.name}.keytab
        chown #{new_resource.owner}:#{new_resource.group} #{new_resource.name}.keytab
        chmod 600 #{new_resource.name}.keytab
      EOH
    end
  end
end
