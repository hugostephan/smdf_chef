#
# Cookbook Name:: shibboleth-idp
# Provider:: relyingparties
#

action :create do
  # Génération du fichier metadata-providers.xml
  template "#{node['shibboleth-idp']['home_dir']}/conf/metadata-providers.xml" do
    source 'metadata-providers.xml.erb'
    mode '0600'
    owner new_resource.owner
    group new_resource.group
    variables(
      :relying_parties => new_resource.relyingparties
    )
    notifies :run, 'execute[reload_shibboleth_metadata_resolver]', :delayed
  end

  # Génération du fichier relying-party.xml
  template "#{node['shibboleth-idp']['home_dir']}/conf/relying-party.xml" do
    source 'relying-party.xml.erb'
    mode '0600'
    owner new_resource.owner
    group new_resource.group
    variables(
      :relying_parties => new_resource.relyingparties
    )
    notifies :run, 'execute[reload_shibboleth_relying_party]', :delayed
  end

  # Création des fichiers de metadonnées de chaque SP
  new_resource.relyingparties.each do |relying_party|
    file "#{node['shibboleth-idp']['home_dir']}/metadata/#{relying_party['id']}-metadata.xml" do
      content Base64.decode64(relying_party['metadata'])
      owner new_resource.owner
      group new_resource.group
      notifies :run, 'execute[reload_shibboleth_metadata_resolver]', :delayed
    end
  end

  # Recharge la conf relying_party.xml
  execute "reload_shibboleth_relying_party" do
    command "./reload-service.sh -id shibboleth.RelyingPartyResolverService"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end

  # Recharge la conf metadata-providers.xml
  execute "reload_shibboleth_metadata_resolver" do
    command "./reload-service.sh -id shibboleth.MetadataResolverService"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end

end

