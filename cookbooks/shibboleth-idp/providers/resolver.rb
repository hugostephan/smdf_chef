#
# Cookbook Name:: shibboleth-idp
# Provider:: resolver
#

action :create do
  # Cas d'un resolver de type LDAP
  if new_resource.properties['type'] == "LDAP"
    # On créer le fichier de properties
    template "#{node['shibboleth-idp']['home_dir']}/conf/attribute-resolver-#{new_resource.name}.xml" do
      source 'attribute-resolver-ldap.xml.erb'
      mode '0600'
      owner new_resource.owner
      group new_resource.group
      variables(
        :attributes => new_resource.properties['attributes'],
        :connector  => new_resource.properties['connector'],
        :name       => new_resource.properties['id']
      )
      notifies :run, 'execute[reload_shibboleth_resolver]', :delayed
    end
  end

  # Recharge la conf attribute resolvers
  execute "reload_shibboleth_resolver" do
    command "./reload-service.sh -id shibboleth.AttributeResolverService"
    cwd "#{node['shibboleth-idp']['home_dir']}/bin"
    user "root"
    group "root"
    action :nothing
    retries 3
    retry_delay 30
  end

end

