#
# Cookbook Name:: shibboleth-idp
# Provider:: metadata
#

use_inline_resources

action :create do

  template "#{node['shibboleth-idp']['home_dir']}/conf/services.xml" do
    source "services.xml.erb"
    owner new_resource.owner
    group new_resource.group
    mode '0600'
    variables(
      :resolvers => new_resource.resolvers.keys
    )
  end

end
