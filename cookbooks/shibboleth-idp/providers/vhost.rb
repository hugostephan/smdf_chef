#
# Cookbook Name:: shibboleth-idp
# Provider:: vhost
#

action :configure do

  # Allow apache to bind to port 8080, by giving it the http_port_t context
  selinux_policy_boolean 'httpd_can_network_connect' do
    value true
  end
  
  # Vhost file creation
  template "#{node['apache']['dir']}/sites-available/#{new_resource.name}.conf" do
    source 'vhost.conf.erb'
    mode '0644'
    owner node['apache']['user']
    group node['apache']['group']
    variables(
      :name => "#{new_resource.name}",
      :proxypass => new_resource.proxypass,
      :proxypassreverse => new_resource.proxypassreverse,
      :proxypreservehost => "#{new_resource.proxypreservehost}",
      :requestheader => new_resource.requestheader,
	  :remoteipheader => new_resource.remoteipheader,
	  :remoteipinternalproxy => new_resource.remoteipinternalproxy,
      :certificatefile => new_resource.certificatefile,
      :certificatekeyfile => new_resource.certificatekeyfile,
      :certificatecafile => new_resource.certificatecafile,
      :ldaptrustedglobalcert => new_resource.ldaptrustedglobalcert,
      :locations => new_resource.locations
    )
    notifies :reload, "service[#{node['apache']['service_name']}]", :delayed
  end

  apache_site "#{new_resource.name}" do
    enable "true"
  end

  service "#{node['apache']['service_name']}" do
    action :nothing
  end

end
