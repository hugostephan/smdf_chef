#
# Cookbook Name:: shibboleth-idp
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

# Requirements
[ 'java', 'jetty', 'apache2', 'apache2::mod_proxy', 'apache2::mod_proxy_http', 'apache2::mod_headers', 'apache2::mod_ssl', 'apache2::mod_rewrite', 'apache2::mod_authnz_ldap', 'apache2::mod_ldap', 'apache2::mod_remoteip', 'selinux_policy::install'].each do |rec|
  include_recipe rec
end


# Installation & Configuration
shibboleth_idp_instance node['shibboleth-idp']['home_dir'] do
  user node['shibboleth-idp']['user']
  group node['shibboleth-idp']['group']
  repository node['shibboleth-idp']['zip_repository'] 
  version node['shibboleth-idp']['version']
  checksum node['shibboleth-idp']['checksum']
  sealer_password get_password('sealer_password')
  keystore_password get_password('keystore_password')
  host_name node['shibboleth-idp']['host_name']
  entity_id node['shibboleth-idp']['entity_id']
  scope node['shibboleth-idp']['scope']
  vhosts node['shibboleth-idp']['vhosts']
  certs_databag node['shibboleth-idp']['certs_databag']
  keys_databag node['shibboleth-idp']['keys_databag']
  authn_databag node['shibboleth-idp']['authn_databag']
  resolvers_databag node['shibboleth-idp']['resolvers_databag']
  filters_databag node['shibboleth-idp']['filters_databag']
  relying_parties_databag node['shibboleth-idp']['relying_parties_databag']
  nameid_databag node['shibboleth-idp']['nameid_databag']
  mysql_storage_databag node['shibboleth-idp']['mysql_storage_databag']
  cas_applications_databag node['shibboleth-idp']['cas_applications_databag']
  nameid_databag node['shibboleth-idp']['nameid_databag']
  action [:install,:configure]
end
