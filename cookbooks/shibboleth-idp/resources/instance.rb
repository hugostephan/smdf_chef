actions :install,:configure
default_action :install

attribute :name,
  :kind_of => String,
  :required => true,
  :name_attribute => true
  
attribute :user,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true

attribute :repository,
  :kind_of => String,
  :required => true

attribute :version,
  :kind_of => String,
  :required => true

attribute :checksum,
  :kind_of => String,
  :required => true

attribute :keystore_password,
  :kind_of => String,
  :required => true

attribute :sealer_password,
  :kind_of => String,
  :required => true

attribute :host_name,
  :kind_of => String,
  :required => true

attribute :entity_id,
  :kind_of => String,
  :required => true

attribute :scope,
  :kind_of => String,
  :required => true

attribute :vhosts,
  :kind_of => Hash,
  :required => true

attribute :passwords_databag,
  :kind_of => String,
  :required => false  
 
attribute :certs_databag,
  :kind_of => String,
  :required => false

attribute :keys_databag,
  :kind_of => String,
  :required => false
  
attribute :authn_databag,
  :kind_of => String,
  :required => false
  
attribute :resolvers_databag,
  :kind_of => String,
  :required => false
  
attribute :filters_databag,
  :kind_of => String,
  :required => false

attribute :relying_parties_databag,
  :kind_of => String,
  :required => false
  
attribute :nameid_databag,
  :kind_of => String,
  :required => false
  
attribute :mysql_storage_databag,
  :kind_of => String,
  :required => false

attribute :cas_applications_databag,
  :kind_of => String,
  :required => false

attribute :nameid_databag,
  :kind_of => String,
  :required => false
