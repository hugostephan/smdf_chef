actions :create

default_action :create

attribute :owner,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true
  
attribute :mysql_storage_databag,
  :kind_of => String,
  :required => true