actions :create

default_action :create

attribute :owner,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true
  
attribute :nameid,
  :kind_of => Array,
  :required => true