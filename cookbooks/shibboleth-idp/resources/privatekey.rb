actions :create

default_action :create

attribute :owner,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true
  
attribute :content,
  :kind_of => [String,Hash],
  :required => true

attribute :format,
  :kind_of => String,
  :required => true