actions :create

default_action :create

attribute :owner,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true
  
attribute :relyingparties,
  :kind_of => Array,
  :required => true