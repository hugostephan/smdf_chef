actions :create

default_action :create

attribute :owner,
  :kind_of => String,
  :required => true

attribute :group,
  :kind_of => String,
  :required => true
  
attribute :properties,
  :kind_of => [String,Hash],
  :required => true