actions :configure

default_action :configure

attribute :proxypass,
  :kind_of => [String,Array]

attribute :proxypassreverse,
  :kind_of => [String,Array]

attribute :proxypreservehost,
  :kind_of => String

attribute :requestheader,
  :kind_of => [String, Array]
  
attribute :remoteipheader,
  :kind_of => Array
  
attribute :remoteipheader,
  :kind_of => Array
  
attribute :remoteipinternalproxy,
  :kind_of => Array

attribute :locations,
  :kind_of => Hash

attribute :certificatefile,
  :kind_of => String

attribute :certificatekeyfile,
  :kind_of => String

attribute :certificatecafile,
  :kind_of => String

attribute :ldaptrustedglobalcert,
  :kind_of => String
