default['shibboleth-sp']['package_name'] = "shibboleth"
default['shibboleth-sp']['dir'] = "/etc/shibboleth"
default['shibboleth-sp']['user'] = "shibd"
default['shibboleth-sp']['service_name'] = "shibd"

default['shibboleh-sp']['enable_syslog'] = false
default['shibboleth-sp']['clockSkew'] = 180

default['shibboleth-sp']['entity_id'] = "https://sp.example.org/shibboleth"
default['shibboleth-sp']['remote_user'] = "persistent"
default['shibboleth-sp']['sign_message'] = "false"
default['shibboleth-sp']['encrypt_message'] = "false"
default['shibboleth-sp']['nameid_format'] = "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"

default['shibboleth-sp']['sessions'] = {
  "handler_url" => "/Shibboleth.sso",
  "lifetime" => 28800,
  "timeout" => 3600,
  "relay_state" => "ss:mem",
  "check_address" => "false",
  "handler_ssl" => "false",
  "cookie_props" => "http",
  "logout" => "SAML2 Local",
}

default['shibboleth-sp']['sso'] = {
  "entity_id" => "https://idp.example.org/idp/shibboleth",
  "discovery_protocol" => "SAMLDS",
  "discovery_url" => "https://ds.example.org/DS/WAYF"
}

default['shibboleth-sp']['handler'] = {
  "status" => {
    "acl" => "127.0.0.1 ::1"
  }
}

default['shibboleth-sp']['errors'] = {
  "support_contact" => "root@#{node['fqdn']}",
  "help_location" => "/about.html",
  "style_sheet" => "/shibboleth-sp/main.css",
  "logo_location" => "/shibboleth-sp/logo.jpg"
}

default['shibboleth-sp']['metadata_providers_databag'] = nil
default['shibboleth-sp']['certificates_databag'] = nil

default['shibboleth-sp']['attribute_map'] = {
  "default" => {
    "name-id" => { 
      "name" => "persistent", 
	  "id" => "persistent"
    },
    "attributes" => 
	[
      {
	    "name" => "urn:oid:0.9.2342.19200300.100.1.1", 
	    "id" => "uid", 
	    "nameFormat" => "basic"
	  }
    ]
  }
}

default['shibboleth-sp']['application_overrides'] = {}