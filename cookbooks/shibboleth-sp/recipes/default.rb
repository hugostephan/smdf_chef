#
# Cookbook Name:: shibboleth-sp
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

metadata_providers = {}
certificates = {}
if node['shibboleth-sp']['metadata_providers_databag']
  metadata_providers = data_bag_item(node['shibboleth-sp']['metadata_providers_databag'],node.chef_environment)['metadata_providers']
end
if node['shibboleth-sp']['certificates_databag']
  certificates = data_bag_item(node['shibboleth-sp']['certificates_databag'],node.chef_environment)['certificates']
end

# Pré-requis : dépôt Shibboleth & Apache
include_recipe "yum-shibboleth"
include_recipe "apache2"

# Installation du SP Shibboleth
package node['shibboleth-sp']['package_name']

# Déploiements des metadonnées
metadata_providers.each do |id,metadata|
  file "#{node['shibboleth-sp']['dir']}/#{id}-metadata.xml" do
    content Base64.decode64(metadata)
    owner "root"
    group "root"
    mode '0644'
	notifies :restart, "service[#{node['shibboleth-sp']['service_name']}]", :delayed
  end
end

# Création du fichier shibboleth2.xml
template "#{node['shibboleth-sp']['dir']}/shibboleth2.xml" do
  user "root"
  group "root"
  mode '0644'
  variables(
      :clockSkew				=> node['shibboleth-sp']['clockSkew'],
	  :entity_id				=> node['shibboleth-sp']['entity_id'],
	  :sign_message				=> node['shibboleth-sp']['sign_message'],
	  :encrypt_message			=> node['shibboleth-sp']['encrypt_message'],
	  :nameid_format			=> node['shibboleth-sp']['nameid_format'],
	  :remote_user				=> node['shibboleth-sp']['remote_user'],
	  :sessions					=> node['shibboleth-sp']['sessions'],
	  :sso						=> node['shibboleth-sp']['sso'],
	  :handler					=> node['shibboleth-sp']['handler'],
	  :errors					=> node['shibboleth-sp']['errors'],
	  :metadata_providers		=> metadata_providers.keys,
	  :application_overrides	=> node['shibboleth-sp']['application_overrides'],
	  :attribute_map			=> node['shibboleth-sp']['attribute_map'],
	  :certificates				=> certificates
    )
  notifies :restart, "service[#{node['shibboleth-sp']['service_name']}]", :delayed
end

# Création des fichiers attribute-map.xml 
node['shibboleth-sp']['attribute_map'].each do |id,map|
  template "#{node['shibboleth-sp']['dir']}/attribute-map-#{id}.xml" do
    source "attribute-map.xml.erb"
    user "root"
    group "root"
    mode '0644'
    variables(
      :attribute_map		=> map
    )
    notifies :restart, "service[#{node['shibboleth-sp']['service_name']}]", :delayed
  end
end

# Déploiement des certificats pour signer/chiffrer les assertions
certificates.each do |cert_name,cert_prop|
  file "#{node['shibboleth-sp']['dir']}/#{cert_name}-cert.pem" do
    content cert_prop['pem']
    owner "root"
    group "root"
    mode '0644'
  end
  
  file "#{node['shibboleth-sp']['dir']}/#{cert_name}-key.pem" do
    content cert_prop['key']
    owner node['shibboleth-sp']['user']
    group "root"
    mode '0400'
  end
end

# Configuration & Activation du module shibd dans Apache
apache_module "shibd" do
  identifier "mod_shib"
  module_path "/usr/lib64/shibboleth/mod_shib_24.so"
end

# On autorise mod_shibd à communiquer avec le socket du service shibd
cookbook_file "#{Chef::Config[:file_cache_path]}/mod_shib-to-shibd.pp" do
  source "mod_shib-to-shibd.pp"
  owner "root"
  group "root"
  action :create
  notifies :run, "execute[install_selinux_module_shibboleth]", :delayed
end

execute "install_selinux_module_shibboleth" do
  command "semodule -i #{Chef::Config[:file_cache_path]}/mod_shib-to-shibd.pp"
  user "root"
  action :nothing
end

# Activation & Démarrage du service
service node['shibboleth-sp']['service_name'] do
  action [:enable, :start]
end