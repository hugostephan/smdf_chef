case node.chef_environment
  when 'development'
    default['smdf-applications']['databases'] = [
      "app_imprimerie_dev",
	  "apps_framework_dev",
	  "reservation_pole_vehicule_dev",
	  "online_macarons_fo_dev"
    ]
  when 'integration'
    default['smdf-applications']['databases'] = [
      "app_imprimerie_int",
	  "apps_framework_int",
	  "reservation_pole_vehicule_int",
	  "online_macarons_fo_int"
    ]
  when 'preproduction'
    default['smdf-applications']['databases'] = [
      "app_imprimerie_preprod",
	  "apps_framework_preprod",
	  "reservation_pole_vehicule_preprod",
	  "online_macarons_fo_preprod"
    ]
  when 'production'
    default['smdf-applications']['databases'] = [
      "app_imprimerie_prod",
	  "apps_framework_prod",
	  "reservation_pole_vehicule_prod",
	  "online_macarons_fo_prod"
    ]
end

default['smdf-applications']['mysql_users_databag'] = nil