#
# Cookbook Name:: smdf-applications
# Recipe:: bash
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#
# This cookbook provides standard librairies and skeletton for smdf bash scripts

# Create common directory if it doesn't exist
execute 'Creating common directory' do
	command 'mkdir -p /opt/smdf/bash/ ; rm -rf /opt/smdf/common-lib.sh ; rm -rf /opt/smdf/skeleton.sh'
end

# Upload bash common-lib
template '/opt/smdf/bash/common-lib.sh' do
  source 'common-lib.sh.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

# Upload bash skeleton
template '/opt/smdf/bash/skeleton.sh' do
  source 'skeleton.sh.erb'
  owner 'root'
  group 'root'
  mode '0644'
end