#
# Cookbook Name:: snmp
# Recipe:: default
#
# Copyright 2010, Eric G. Wolfe
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

node['snmp']['packages'].each do |snmppkg|
  package snmppkg
end

template '/etc/default/snmpd' do
  mode 0644
  owner 'root'
  group 'root'
  only_if { node['platform_family'] == 'debian' }
end

service node['snmp']['service'] do
  action [:start, :enable]
end

groupnames = []
node['snmp']['groups']['v1'].each_key { |key| groupnames << key }
node['snmp']['groups']['v2c'].each_key { |key| groupnames << key }
groupnames = groupnames.uniq

template '/etc/snmp/snmpd.conf' do
  mode 00600
  owner 'root'
  group 'root'
  variables(groups: groupnames)
  notifies :restart, "service[#{node['snmp']['service']}]"
end

# IPTables config to accept SNMP queries
execute 'IPTables delete potential existing port 161 rule' do
	command 'iptables -D INPUT -p udp -m udp --dport 161 -j ACCEPT || true'
end

execute 'IPTables open port 161' do
	command 'iptables -I INPUT -p udp -m udp --dport 161 -j ACCEPT'
end

# IPTables config to accept SNMP queries
execute 'IPTables delete potential existing port 162 rule' do
	command 'iptables -D INPUT -p udp -m udp --dport 162 -j ACCEPT || true'
end

execute 'IPTables open port 162' do
	command 'iptables -I INPUT -p udp -m udp --dport 162 -j ACCEPT'
end

# Saving iptable
execute 'Saving IPTables' do
	command 'iptables-save > /etc/sysconfig/iptables'
end

# Enabling SNMP access
execute 'Opening SNMP port 161 & 162 on firewall' do
	command 'firewall-cmd --permanent --add-port=161/udp || true ; firewall-cmd --permanent --add-port=162/udp || true ; firewall-cmd --reload || true'
end
