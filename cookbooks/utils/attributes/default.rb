# Package � installer
default['utils']['packages']['default']['bzip2'] = "bzip2"
default['utils']['packages']['default']['unzip'] = "unzip"
default['utils']['packages']['default']['cifs'] = "cifs-utils"
default['utils']['packages']['default']['net-tools'] = "net-tools"
default['utils']['packages']['default']['traceroute'] = "traceroute"