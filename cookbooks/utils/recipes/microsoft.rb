#
# Cookbook Name:: utils
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

node['utils']['packages']['microsoft'].each do |key,value|
	package value
end

# Configure odbcinst
template '/etc/odbcinst.ini' do
  source 'odbcinst.ini.erb'
  owner 'root'
  group 'root'
  mode '0644'
end