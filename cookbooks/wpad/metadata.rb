name             'wpad'
maintainer       'Mairie_Saint-Maur'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures Wpad file for net config auto detection'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
