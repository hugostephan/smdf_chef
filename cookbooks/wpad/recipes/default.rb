#
# Cookbook Name:: smdf-applications
# Recipe:: default
#
# Copyright 2016, Mairie_Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

template "#{node['wpad']['location_dir']}/wpad.dat" do
  source "#{node['wpad']['template']}.erb"
  cookbook node['wpad']['cookbook']
  unless platform?('windows')
    owner 'root'
    group node['root_group']
    mode '0644'
  end
end