#
# Cookbook Name:: x11vnc
# Recipe:: default
#
# Copyright 2018, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Deleting potential previous install (bad idea - will keep uninstalling and reinstalling x11vnc)
# execute 'deleting deprecated package' do
  # command 'yum remove x11vnc -y'
# end

# Adding GDM config to authorize remote X access
template '/etc/gdm/custom.conf' do
  source 'custom.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

# Adding iptables rules for VNC connections
execute 'configuring iptables for VNC:5900' do
  command '/usr/sbin/iptables -A IN_public_allow -p tcp -m tcp --dport 5900 -m conntrack --ctstate NEW -j ACCEPT ; /usr/sbin/iptables-save > /etc/sysconfig/iptables'
end

# Adding firewalld rules for VNC connections
execute 'configure firewalld for VNC:5900' do
  command 'firewall-cmd --add-port 5900/tcp --permanent'
end

# Download & install our custom x11vnc package
execute 'Download and install x11vnc IPv4' do
  command 'mkdir -p /var/log/x11vnc/ ; chmod -R 777 /var/log/x11vnc/ ; mkdir -p /opt/x11vnc/ && cd /opt/x11vnc ; [ -f /usr/bin/x11vnc ] && echo "custom x11vnc already installed" || (wget http://repo-p.dmz.saintmaur.local/x11vnc/x11vnc.rpm ; yum install -y x11vnc.rpm)'
end

# Adding GDM config to authorize remote X access
template '/opt/x11vnc/assistance' do
  source 'assistance.erb'
  owner 'root'
  group 'root'
  mode '0777'
end

# Adding script shortcut to every user desktop
execute 'Adding x11vnc shortcut to default profile' do
  command '[ -f /opt/x11vnc/shortcut.default ] && echo "OK" || (echo "cp -rf /opt/x11vnc/assistance ~/Desktop/Assistance > /dev/null 2>&1" > /etc/profile.d/assistance.sh ; chmod 777 /etc/profile.d/assistance.sh ; touch /opt/x11vnc/shortcut.default)'
end
execute 'Adding x11vnc shortcut to default X11 session' do
  command '[ -f /opt/x11vnc/shortcut.x11 ] && echo "OK" || (mkdir -p /etc/X11/Xsession.d/ ; echo "cp -rf /opt/x11vnc/assistance ~/Desktop/Assistance" > /etc/X11/XSession.d/assistance.sh ; chmod 777 etc/X11/XSession.d/assistance.sh ; touch /opt/x11vnc/shortcut.x11)'
end
