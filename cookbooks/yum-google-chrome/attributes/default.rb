default['yum']['google-chrome']['repositoryid'] = 'google-chrome'
 
default['yum']['google-chrome']['description'] = 'Google Chrome repo'
default['yum']['google-chrome']['baseurl'] = 'http://repo-p.dmz.saintmaur.local/centos/7/google-chrome/' 
default['yum']['google-chrome']['gpgkey'] = 'http://repo-p.dmz.saintmaur.local/centos/7/google-chrome/repodata/repomd.xml'

default['yum']['google-chrome']['failovermethod'] = 'priority'
default['yum']['google-chrome']['gpgcheck'] = true
default['yum']['google-chrome']['enabled'] = true