#
# Cookbook Name:: yum-google-chrome
# Recipe:: default
#
# Copyright 2018, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

yum_repository 'google-chrome' do
    description node['yum']['google-chrome']['description']
    baseurl node['yum']['google-chrome']['baseurl']
    mirrorlist node['yum']['google-chrome']['mirrorlist']
    gpgcheck node['yum']['google-chrome']['gpgcheck']
    gpgkey node['yum']['google-chrome']['gpgkey']
    enabled node['yum']['google-chrome']['enabled']
    failovermethod node['yum']['google-chrome']['failovermethod']
    http_caching node['yum']['google-chrome']['http_caching']
    include_config node['yum']['google-chrome']['include_config']
    includepkgs node['yum']['google-chrome']['includepkgs']
    keepalive node['yum']['google-chrome']['keepalive']
    max_retries node['yum']['google-chrome']['max_retries']
    metadata_expire node['yum']['google-chrome']['metadata_expire']
    mirror_expire node['yum']['google-chrome']['mirror_expire']
    priority node['yum']['google-chrome']['priority']
    proxy node['yum']['google-chrome']['proxy']
    proxy_username node['yum']['google-chrome']['proxy_username']
    proxy_password node['yum']['google-chrome']['proxy_password']
    repositoryid node['yum']['google-chrome']['repositoryid']
    sslcacert node['yum']['google-chrome']['sslcacert']
    sslclientcert node['yum']['google-chrome']['sslclientcert']
    sslclientkey node['yum']['google-chrome']['sslclientkey']
    sslverify node['yum']['google-chrome']['sslverify']
    timeout node['yum']['google-chrome']['timeout']
    action :create
end
