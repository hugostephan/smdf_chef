default['yum']['google']['repositoryid'] = 'google'

default['yum']['google']['description'] = 'google'
default['yum']['google']['baseurl'] = 'http://dl.google.com/linux/chrome/rpm/stable/$basearch'
default['yum']['google']['gpgkey'] = 'https://dl-ssl.google.com/linux/linux_signing_key.pub'

default['yum']['google']['failovermethod'] = 'priority'
default['yum']['google']['gpgcheck'] = node['yum']['google']['gpgcheck']
default['yum']['google']['enabled'] = true