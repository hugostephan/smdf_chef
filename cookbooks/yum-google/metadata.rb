name             'yum-google'
maintainer       'mairie-saint-maur'
maintainer_email 'blaise.thauvin@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures yum-google'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
