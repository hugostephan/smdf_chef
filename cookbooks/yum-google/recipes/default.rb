#
# Cookbook Name:: yum-google
# Recipe:: default
#
# Copyright 2017, Ville de Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

yum_repository 'google' do
    description node['yum']['google']['description']
    baseurl node['yum']['google']['baseurl']
    mirrorlist node['yum']['google']['mirrorlist']
    gpgcheck node['yum']['google']['gpgcheck']
    gpgkey node['yum']['google']['gpgkey']
    enabled node['yum']['google']['enabled']
    failovermethod node['yum']['google']['failovermethod']
    http_caching node['yum']['google']['http_caching']
    include_config node['yum']['google']['include_config']
    includepkgs node['yum']['google']['includepkgs']
    keepalive node['yum']['google']['keepalive']
    max_retries node['yum']['google']['max_retries']
    metadata_expire node['yum']['google']['metadata_expire']
    mirror_expire node['yum']['google']['mirror_expire']
    priority node['yum']['google']['priority']
    proxy node['yum']['google']['proxy']
    proxy_username node['yum']['google']['proxy_username']
    proxy_password node['yum']['google']['proxy_password']
    repositoryid node['yum']['google']['repositoryid']
    sslcacert node['yum']['google']['sslcacert']
    sslclientcert node['yum']['google']['sslclientcert']
    sslclientkey node['yum']['google']['sslclientkey']
    sslverify node['yum']['google']['sslverify']
    timeout node['yum']['google']['timeout']
    action :create
end
