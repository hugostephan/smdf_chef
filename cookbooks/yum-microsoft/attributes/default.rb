default['yum']['microsoft']['repositoryid'] = 'packages-microsoft-com-prod'

default['yum']['microsoft']['description'] = 'Microsoft packages such mssql connectors'
default['yum']['microsoft']['baseurl'] = node['yum']['packages-microsoft-com-prod']['baseurl'] #'https://packages.microsoft.com/rhel/7/prod/'
default['yum']['microsoft']['gpgkey'] = node['yum']['packages-microsoft-com-prod']['gpgkey'] #'https://packages.microsoft.com/keys/microsoft.asc'

default['yum']['microsoft']['failovermethod'] = 'priority'
default['yum']['microsoft']['gpgcheck'] = node['yum']['packages-microsoft-com-prod']['gpgcheck']
default['yum']['microsoft']['enabled'] = true