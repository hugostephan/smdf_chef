name             'yum-microsoft'
maintainer       'mairie-saint-maur'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures yum microsoft repo'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
