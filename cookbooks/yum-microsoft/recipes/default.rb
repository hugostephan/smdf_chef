#
# Cookbook Name:: yum-microsoft
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# execute 'microsoft_repo_config' do
	# command "curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/msprod.repo"
# end

# http_request 'microsoft repo request' do
  # url "https://packages.microsoft.com/config/rhel/7/prod.repo"
  # message <xml request>
  # action :post
# end

yum_repository 'microsoft' do
    description node['yum']['microsoft']['description']
    baseurl node['yum']['microsoft']['baseurl']
    mirrorlist node['yum']['microsoft']['mirrorlist']
    gpgcheck node['yum']['microsoft']['gpgcheck']
    gpgkey node['yum']['microsoft']['gpgkey']
    enabled node['yum']['microsoft']['enabled']
	exclude node['yum']['packages-microsoft-com-prod']['exclude']
    failovermethod node['yum']['microsoft']['failovermethod']
    http_caching node['yum']['microsoft']['http_caching']
    include_config node['yum']['microsoft']['include_config']
    includepkgs node['yum']['microsoft']['includepkgs']
    keepalive node['yum']['microsoft']['keepalive']
    max_retries node['yum']['microsoft']['max_retries']
    metadata_expire node['yum']['microsoft']['metadata_expire']
    mirror_expire node['yum']['microsoft']['mirror_expire']
    priority node['yum']['microsoft']['priority']
    proxy node['yum']['microsoft']['proxy']
    proxy_username node['yum']['microsoft']['proxy_username']
    proxy_password node['yum']['microsoft']['proxy_password']
    repositoryid node['yum']['microsoft']['repositoryid']
    sslcacert node['yum']['microsoft']['sslcacert']
    sslclientcert node['yum']['microsoft']['sslclientcert']
    sslclientkey node['yum']['microsoft']['sslclientkey']
    sslverify node['yum']['microsoft']['sslverify']
    timeout node['yum']['microsoft']['timeout']
    action :create
end
