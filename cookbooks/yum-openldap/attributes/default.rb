default['yum']['openldap']['repositoryid'] = 'openldap'


case node['platform_version'].to_i
  when 5
    default['yum']['openldap']['description'] = 'LTB project packages'
    default['yum']['openldap']['baseurl'] = 'http://ltb-project.org/rpm/$releasever/$basearch'
    default['yum']['openldap']['gpgkey'] = 'http://ltb-project.org/wiki/lib/RPM-GPG-KEY-LTB-project'
  when 6
    default['yum']['openldap']['description'] = 'LTB project packages'
    default['yum']['openldap']['baseurl'] = 'http://ltb-project.org/rpm/$releasever/$basearch'
    default['yum']['openldap']['gpgkey'] = 'http://ltb-project.org/wiki/lib/RPM-GPG-KEY-LTB-project'
  when 7
    default['yum']['openldap']['description'] = 'LTB project packages'
    default['yum']['openldap']['baseurl'] = 'http://ltb-project.org/rpm/$releasever/$basearch'
    default['yum']['openldap']['gpgkey'] = 'http://ltb-project.org/wiki/lib/RPM-GPG-KEY-LTB-project'
end


default['yum']['openldap']['failovermethod'] = 'priority'
default['yum']['openldap']['gpgcheck'] = true
default['yum']['openldap']['enabled'] = false
default['yum']['openldap']['managed'] = false