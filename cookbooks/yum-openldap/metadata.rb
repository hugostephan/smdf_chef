name             'yum-openldap'
maintainer       'Mairie_Saint_Maur'
maintainer_email 'julien.huon@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures yum-openldap'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'yum', '>= 3.11.0'