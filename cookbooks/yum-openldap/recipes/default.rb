#
# Cookbook Name:: yum-openldap
# Recipe:: default
#
# Copyright 2016, Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

if node['yum']['openldap']['managed']
    yum_repository 'openldap' do
        description node['yum']['openldap']['description']
        baseurl node['yum']['openldap']['baseurl']
        mirrorlist node['yum']['openldap']['mirrorlist']
        gpgcheck node['yum']['openldap']['gpgcheck']
        gpgkey node['yum']['openldap']['gpgkey']
        enabled node['yum']['openldap']['enabled']
        cost node['yum']['openldap']['cost']
        exclude node['yum']['openldap']['exclude']
        enablegroups node['yum']['openldap']['enablegroups']
        failovermethod node['yum']['openldap']['failovermethod']
        http_caching node['yum']['openldap']['http_caching']
        include_config node['yum']['openldap']['include_config']
        includepkgs node['yum']['openldap']['includepkgs']
        keepalive node['yum']['openldap']['keepalive']
        max_retries node['yum']['openldap']['max_retries']
        metadata_expire node['yum']['openldap']['metadata_expire']
        mirror_expire node['yum']['openldap']['mirror_expire']
        priority node['yum']['openldap']['priority']
        proxy node['yum']['openldap']['proxy']
        proxy_username node['yum']['openldap']['proxy_username']
        proxy_password node['yum']['openldap']['proxy_password']
        repositoryid node['yum']['openldap']['repositoryid']
        sslcacert node['yum']['openldap']['sslcacert']
        sslclientcert node['yum']['openldap']['sslclientcert']
        sslclientkey node['yum']['openldap']['sslclientkey']
        sslverify node['yum']['openldap']['sslverify']
        timeout node['yum']['openldap']['timeout']
        action :create
    end
end