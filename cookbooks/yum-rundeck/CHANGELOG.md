# yum-rundeck CHANGELOG

This file is used to list changes made in each version of the yum-rundeck cookbook.

## 0.1.0
- [huon-jul] - Initial release of yum-rundeck

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
