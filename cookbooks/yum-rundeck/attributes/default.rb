default['yum']['rundeck']['repositoryid'] = 'rundeck'


default['yum']['rundeck']['description'] = 'Rundeck - Release'
default['yum']['rundeck']['baseurl'] = 'http://dl.bintray.com/rundeck/rundeck-rpm'

default['yum']['rundeck']['failovermethod'] = 'priority'
default['yum']['rundeck']['gpgcheck'] = false
default['yum']['rundeck']['enabled'] = false
default['yum']['rundeck']['managed'] = false