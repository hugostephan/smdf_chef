#
# Cookbook Name:: yum-rundeck
# Recipe:: default
#
# Copyright 2016, Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

if node['yum']['rundeck']['managed']
    yum_repository 'rundeck' do
        description node['yum']['rundeck']['description']
        baseurl node['yum']['rundeck']['baseurl']
        mirrorlist node['yum']['rundeck']['mirrorlist']
        gpgcheck node['yum']['rundeck']['gpgcheck']
        gpgkey node['yum']['rundeck']['gpgkey']
        enabled node['yum']['rundeck']['enabled']
        cost node['yum']['rundeck']['cost']
        exclude node['yum']['rundeck']['exclude']
        enablegroups node['yum']['rundeck']['enablegroups']
        failovermethod node['yum']['rundeck']['failovermethod']
        http_caching node['yum']['rundeck']['http_caching']
        include_config node['yum']['rundeck']['include_config']
        includepkgs node['yum']['rundeck']['includepkgs']
        keepalive node['yum']['rundeck']['keepalive']
        max_retries node['yum']['rundeck']['max_retries']
        metadata_expire node['yum']['rundeck']['metadata_expire']
        mirror_expire node['yum']['rundeck']['mirror_expire']
        priority node['yum']['rundeck']['priority']
        proxy node['yum']['rundeck']['proxy']
        proxy_username node['yum']['rundeck']['proxy_username']
        proxy_password node['yum']['rundeck']['proxy_password']
        repositoryid node['yum']['rundeck']['repositoryid']
        sslcacert node['yum']['rundeck']['sslcacert']
        sslclientcert node['yum']['rundeck']['sslclientcert']
        sslclientkey node['yum']['rundeck']['sslclientkey']
        sslverify node['yum']['rundeck']['sslverify']
        timeout node['yum']['rundeck']['timeout']
        action :create
    end
end