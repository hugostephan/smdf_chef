# yum-shibboleth CHANGELOG

This file is used to list changes made in each version of the yum-shibboleth cookbook.

## 0.1.0
- [huon-jul] - Initial release of yum-shibboleth

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
