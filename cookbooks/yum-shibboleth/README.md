# yum-shibboleth Cookbook

Permet d'ajouter les dépots Shibboleth à CentOS.


## Requirements

### Platforms

- CentOS 7

### Chef

- Chef 12.0 or later

### Cookbooks

- yum

## Attributes


## Usage

### yum-shibboleth::default

Just include `yum-shibboleth` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[yum-shibboleth]"
  ]
}
```

## Contributing

Allez, viens contribuer, on est bien ! 

## License and Authors

Authors: Julien Huon (julien.huon@mairie-saint-maur.com)

