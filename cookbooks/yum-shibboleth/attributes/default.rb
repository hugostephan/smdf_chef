default['yum']['shibboleth']['repositoryid'] = 'shibboleth'


case node['platform_version'].to_i
  when 5
    default['yum']['shibboleth']['description'] = 'LTB project packages'
    default['yum']['shibboleth']['baseurl'] = 'http://download.opensuse.org/repositories/security:/shibboleth/CentOS_5/'
    default['yum']['shibboleth']['gpgkey'] = 'http://download.opensuse.org/repositories/security:/shibboleth/CentOS_5//repodata/repomd.xml.key'
  when 6
    default['yum']['shibboleth']['description'] = 'LTB project packages'
    default['yum']['shibboleth']['baseurl'] = 'http://download.opensuse.org/repositories/security:/shibboleth/CentOS_CentOS-6/'
    default['yum']['shibboleth']['gpgkey'] = 'http://download.opensuse.org/repositories/security:/shibboleth/CentOS_CentOS-6//repodata/repomd.xml.key'
  when 7
    default['yum']['shibboleth']['description'] = 'LTB project packages'
    default['yum']['shibboleth']['baseurl'] = 'http://download.opensuse.org/repositories/security:/shibboleth/CentOS_7/'
    default['yum']['shibboleth']['gpgkey'] = 'http://download.opensuse.org/repositories/security:/shibboleth/CentOS_7//repodata/repomd.xml.key'
end


default['yum']['shibboleth']['failovermethod'] = 'priority'
default['yum']['shibboleth']['gpgcheck'] = true
default['yum']['shibboleth']['enabled'] = false
default['yum']['shibboleth']['managed'] = false