#
# Cookbook Name:: yum-shibboleth
# Recipe:: default
#
# Copyright 2016, Saint-Maur
#
# All rights reserved - Do Not Redistribute
#

if node['yum']['shibboleth']['managed']
    yum_repository 'shibboleth' do
        description node['yum']['shibboleth']['description']
        baseurl node['yum']['shibboleth']['baseurl']
        mirrorlist node['yum']['shibboleth']['mirrorlist']
        gpgcheck node['yum']['shibboleth']['gpgcheck']
        gpgkey node['yum']['shibboleth']['gpgkey']
        enabled node['yum']['shibboleth']['enabled']
        cost node['yum']['shibboleth']['cost']
        exclude node['yum']['shibboleth']['exclude']
        enablegroups node['yum']['shibboleth']['enablegroups']
        failovermethod node['yum']['shibboleth']['failovermethod']
        http_caching node['yum']['shibboleth']['http_caching']
        include_config node['yum']['shibboleth']['include_config']
        includepkgs node['yum']['shibboleth']['includepkgs']
        keepalive node['yum']['shibboleth']['keepalive']
        max_retries node['yum']['shibboleth']['max_retries']
        metadata_expire node['yum']['shibboleth']['metadata_expire']
        mirror_expire node['yum']['shibboleth']['mirror_expire']
        priority node['yum']['shibboleth']['priority']
        proxy node['yum']['shibboleth']['proxy']
        proxy_username node['yum']['shibboleth']['proxy_username']
        proxy_password node['yum']['shibboleth']['proxy_password']
        repositoryid node['yum']['shibboleth']['repositoryid']
        sslcacert node['yum']['shibboleth']['sslcacert']
        sslclientcert node['yum']['shibboleth']['sslclientcert']
        sslclientkey node['yum']['shibboleth']['sslclientkey']
        sslverify node['yum']['shibboleth']['sslverify']
        timeout node['yum']['shibboleth']['timeout']
        action :create
    end
end