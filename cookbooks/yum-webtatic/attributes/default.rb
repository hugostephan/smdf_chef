default['yum']['webtatic']['repositoryid'] = 'webtatic'
default['yum']['webtatic']['description'] = 'Web oriented packages'
default['yum']['webtatic']['mirrorlist'] = node['yum']['webtatic']['mirrorlist']

default['yum']['webtatic']['gpgcheck'] = node['yum']['webtatic']['gpgcheck']
default['yum']['webtatic']['enabled'] = true