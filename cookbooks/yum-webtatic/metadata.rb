name             'yum-webtatic'
maintainer       'mairie-saint-maur'
maintainer_email 'hugo.stephan@mairie-saint-maur.com'
license          'All rights reserved'
description      'Installs/Configures yum webtatic repo'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
