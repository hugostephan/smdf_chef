#
# Cookbook Name:: yum-webtatic
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
yum_repository 'webtatic' do
    description node['yum']['webtatic']['description']
    baseurl node['yum']['webtatic']['baseurl']
    mirrorlist node['yum']['webtatic']['mirrorlist']
    gpgcheck node['yum']['webtatic']['gpgcheck']
    gpgkey node['yum']['webtatic']['gpgkey']
    enabled node['yum']['webtatic']['enabled']
    failovermethod node['yum']['webtatic']['failovermethod']
    http_caching node['yum']['webtatic']['http_caching']
    include_config node['yum']['webtatic']['include_config']
    includepkgs node['yum']['webtatic']['includepkgs']
    keepalive node['yum']['webtatic']['keepalive']
    max_retries node['yum']['webtatic']['max_retries']
    metadata_expire node['yum']['webtatic']['metadata_expire']
    mirror_expire node['yum']['webtatic']['mirror_expire']
    priority node['yum']['webtatic']['priority']
    proxy node['yum']['webtatic']['proxy']
    proxy_username node['yum']['webtatic']['proxy_username']
    proxy_password node['yum']['webtatic']['proxy_password']
    repositoryid node['yum']['webtatic']['repositoryid']
    sslcacert node['yum']['webtatic']['sslcacert']
    sslclientcert node['yum']['webtatic']['sslclientcert']
    sslclientkey node['yum']['webtatic']['sslclientkey']
    sslverify node['yum']['webtatic']['sslverify']
    timeout node['yum']['webtatic']['timeout']
    action :create
end
