#
# Author:: Hugo STEPHAN (hugo.stephan@mairie-saint-maur.com)
# Recipe:: yum::update
#
# Copyright 2017-2018, Mairie saint maur des fosses
#

# If auto update is ON
if node['yum']['update']['on']
  cmd = 'yum -y update --skip-broken '

  # Excluding packets from auto update
  if node['yum']['update']['excluded']
    cmd += ' --exclude='
    cpt = 0

    node['yum']['update']['excluded'].each do |package|
      cmd += case cpt
             when 0
               package
             else
               ',' + package
             end
      cpt += 1
    end

  end

  # Running update
  execute 'checking for updates' do
    command cmd
  end
end

# Cleaning yum anyway
execute 'cleaning up yum' do
  command 'yum clean all'
end
execute 'cleaning yum cache' do
  command 'rm -rf /var/cache/yum'
end
execute 'cleaning up rpm db' do
  command 'rpm -Va --nofiles --nodigest'
end
execute 'rebuilding yum db' do
  command 'rpm --rebuilddb'
end


# If auto update is ON
if node['yum']['update']['on']
  cmd = 'yum -y update'

  # Excluding packets from auto update
  if node['yum']['update']['excluded']
    cmd += ' --exclude='
    cpt = 0

    node['yum']['update']['excluded'].each do |package|
      cmd += case cpt
             when 0
               package
             else
               ',' + package
             end
      cpt += 1
    end

  end

  # Running update
  execute 'checking for updates' do
    command cmd
  end

else
  Chef::Log.info('Auto update is disabled for this server')
end
